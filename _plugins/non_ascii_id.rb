require "jekyll-commonmark-ghpages"

class JekyllCommonMarkCustomRenderer
  module NonASCIIID
    private
    def basic_generate_id(str)
      str.downcase.gsub(" ", "-")
    end
  end

  prepend NonASCIIID
end
