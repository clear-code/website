---
title: Techouseさまにおける新人研修の一環としてのOSS Gateワークショップ実施事例紹介
author: piro_or
tags:
  - use-case
--- 
 
当社のOSS開発サポート事業では、企業におけるオープンソースに関連したお困りごとの解決のために、開発への協力、研修の実施など、様々なお手伝いをしています。

去る4月18日、頂いたご相談に基づき、Webサービスを自社開発・運営しておられる企業の[Techouseさま](https://jp.techouse.com/)において、新人研修の一環としてOSS Gateワークショップを実施しました。
[参加された方による体験記の記事がすでにTechouseさまの開発者向けブログで公開されています](https://developers.techouse.com/entry/OSS-Gate-Workshop-2024)ので、ワークショップがどのような様子だったかはぜひそちらをご覧下さい。  
![（研修参加者の皆さんが各自で作業を進めている様子の写真）]({% link /images/blog/techouse-oss-gate-workshop-report/01.jpg %})

本記事では開催側の視点から、何を期待し、どのように効果を測定したか、どのような準備を行ったかをご紹介します。
*OSS開発に関わってみたいけれどもうまく進められておらず困っている、当社のOSS開発サポートで具体的にどのようなサポートを受けられるのかを知りたい、とお考えの企業さま*に参考にして頂けましたら幸いです。


（本記事に掲載の写真画像はTechouseさまからご提供を頂いた物で、CC BY-SA 4.0およびGFDLの適用外となります。記事を再利用される際は、写真画像を省いてお使いください。）


<!--more-->

### Techouseさまの期待と現状

元々Techouseさまでは、業務には直結しない話題も含めた様々な内容の勉強会を社内で開催するなど、所属ソフトウェアエンジニアの方々の技術力の維持・向上のために様々な取り組みをされているそうです。
当初は、そういった取り組みの題材の1つとして、何かしらの刺激の源としてオープンソース活動を社内で行いたい・推進したい、ということでTechouseさまから支援のご相談を頂きました。

技術力向上や刺激を得るという観点で、「実際のOSS開発プロジェクトに開発者として関わる」ことには確かにいくつもメリットを見いだせます。

* わざわざ教材を用意せずとも、未解決の問題が多数ある。（ただし、粒度や難易度は千差万別）
  * OSS開発に関わり続けるだけで、継続的に刺激を得られると期待できる。
* 今までブラックボックスとして触れていた既存のOSSについて、その内側を覗く機会ができる。
  * 業務上で遭遇したOSSの不具合について、深掘りして調査できるようになると、より良い解決策が見付かる可能性が増す。
  * 過去の経緯を知らない状態でコードに触れて調査したり、既存のコードに合わせたコードを書いたりする訓練になる。

Techouseさま所属のエンジニアの方の中には、障害報告やプルリクエスト作成などを行ったことのある方もおられます。
また、業務で使用するライブラリーなどはなるべく最新の物を使うようにされているとのことでしたので、最新版に特有の不具合を報告したり修正の提案をしたりできれば、業務の中で無理なくオープンソース活動を行えそうに思えました。
しかし実際には、「通常の開発業務で多忙で、時間を取れない」ことが主な理由で、オープンソース活動はあまり行われていないのが現状とのことでした。

以上の事を踏まえ、単発で実施しやすい施策として、今回はOSS Gateワークショップの実施をご提案しました。


### 新人研修としてのOSS Gateワークショップ実施のご提案

#### OSS Gateワークショップとは

OSS Gateワークショップは、当社も参与している[OSS Gateというコミュニティ](https://oss-gate.github.io/)で行っている、「実際に何らかのOSSを使用して、その過程で遭遇した躓きを実際に開発プロジェクトにフィードバックする」という形で*オープンソース開発の最初の一歩を体験すること*に特化したワークショップです。
当社ではこれまでにも複数回、企業内での研修として実施してきた実績があります。

このワークショップをコミュニティで開催する際には、「体験すること」以上の目標を強くは掲げていないため、参加者個々人によって何を持ち帰るかは異なります。
「何でもいいからオープンソース活動の実績を作る（増やす）」を目標にする人もいれば、「特定のオープンソース開発プロジェクトにコントリビュートすること」を目標にする人もいます。


#### 今回ワークショップで得たい物

前述したTechouseさま社内の現状から、Techouseさまにおいては「オープンソース活動は、業務の片手間でやるには時間や手間がかかりすぎるものだ」と認識されている方が多い様子が窺えました。

筆者の実感では、オープンソース活動の難易度には、15分程度で済む物から数日がかりの物まで大きな幅があります。
これまでOSS Gateワークショップ参加者の方々にお話を伺った感触からは、オープンソース活動未経験の方ほど、前述の幅の中で最も難しく手間がかかるケースを想定されている様子でした。
ただ、そういう方でも、ワークショップ体験後には「思っていたより簡単だった」と認識を改められることが多いとも感じます。

将来的には継続的なオープンソース活動から様々な刺激を得て頂けるようになるとよいのですが、その前段階として、まずは*オープンソース活動を「自分に取れる行動の選択肢の1つ」として考えて頂けるようになる*必要があります。
そのような考えから、今回は「『実際にオープンソース開発プロジェクトへフィードバックする』体験を通じて、*『やってみたら、それほど難しくなかった』『これなら、自分にも普通にできそう』という感想につながるような刺激*を、新人の方々に得て頂く」ことを目標に設定し、OSS Gateワークショップの実施をご提案したのでした。


### ワークショップのカスタマイズ

#### 内容の変更

OSS Gateワークショップは通常、以下のようなタイムスケジュールで進行しています。

* 初対面の参加者同士のアイスブレイク（15分）
* 事前説明の座学パート（30分）
* 実習パート前半（作業60分、ふりかえり15分）
* 休憩（60分）
* 実習パート後半（作業120分、休憩10分、ふりかえり15分）
* 全体のまとめ説明（10分）
* ワークショップふりかえり（アンケート記入10分、ふりかえり45分）

今回のワークショップでは、参加者の皆さん（新人の方々）はすでに実務経験があり、作業パートでは手間取ることなく進行できそうだったことから、作業パートに割り当てる時間を少し減らして、座学パート部分を[フィヨルドブートキャンプさま向けの開催時の内容](https://slide.rabbit-shocker.org/authors/Piro/presentation-oss-gate-workshop-fjord-bootcamp-introduction/)ベースで厚めにすることにしました。
参加者同士は初対面ではないことから、アイスブレイクの時間も省略することにしました。

また、最後のアンケート回答とそれを踏まえたふりかえりの時間についても、割り当てを変更することにしました。
コミュニティ活動としての通常開催のOSS Gateワークショップでは、終了後のふりかえりはコミュニティ活動の改善を目的としています。
しかし、今回はTechouseさまの新人研修としての実施で、研修の改善は主催サイドが取り組んでいくべき課題であることや、参加者向けの聞き取りは別途時間を取って行える[^postprocess]ことから、ワークショップの時間をそこに充てる必要性は薄いと考え、今回は代わりに「どうすれば社内のオープンソース活動を盛り立てていけるか」を考えるオープンディスカッションを行うことにしました。

[^postprocess]: 通常開催では「後日改めて聞き取りをする」ことが難しいため、ワークショップの時間の中で一度にやってしまっています。本来なら、ある程度時間を置いて、ワークショップでの興奮状態から醒めた状態で聞き取りを行った方が、より有用な情報を得られるとは思っているのですが……

これらのカスタマイズを行ったタイムスケジュール案は、以下の要領となりました。

* 事前説明の座学パート（前半30分、後半30分）
* 実習パート前半（作業60分、ふりかえり15分）
* 休憩（60分）
* 実習パート後半（100分）
* 小休憩（10分）
* ふりかえり（テーブル内ふりかえり10分、全体共有15分）
* オープンディスカッション（10分）
* 全体のまとめ説明（10分）
* アンケート記入（10分）

これを基本として、実習パート後半が順調に進みすぎてダレた空気になってしまうようであれば、実習パートを早めに切り上げてオープンディスカッションの時間を長めにするなどの調整をする想定で当日に挑むことにしました。


#### 効果測定

業務としてお請けするにあたっては、実施した施策の効果を測定する必要があります。
効果の有無が分からないと、お客さまにとっても「今後同様の施策を続けていくべきかどうか」を判断できません。

今回の目標は「ワークショップを通じて『やってみたら、それほど難しくなかった』『これなら、自分にも普通にできそう』という感想につながるような刺激を得て頂くこと」なので、目標を達成できたかどうかは「刺激を得られたかどうか」「刺激によって意識がどう変化したか」「期待されたような感想を持ってもらえたかどうか」という尺度で測ることになります。
その手段として、ワークショップの実施前後で同じ内容のアンケートに回答して頂き、アンケート結果を比較することにしました。
また、ワークショップの実施後にある程度期間を置いてTechouseさま主導で行って頂いたヒアリング結果も、効果測定の材料となりました。


#### 実施形態の決定

OSS Gateワークショップは、オフライン開催とオンライン開催の2形態があります。

オフライン開催の利点は、密なコミュニケーションが可能となることです。
サポーター[^supporter]1名につきビギナー[^beginner]3名という組み合わせでの安定した実施が可能で、サポーターの人数が少なくても無理なく実施しやすいですし、ビギナー参加者同士での助け合いや情報共有の機会も生じます。  
![（オフライン開催の形態のワークショップにおいて、研修参加者の皆さんが各自で作業を進めている様子の写真）]({% link /images/blog/techouse-oss-gate-workshop-report/02.jpg %})

[^supporter]: ワークショップの時間中でいわゆるメンター側の人のこと。
[^beginner]: ワークショップの時間中でオープンソース活動を体験してみる側の人のこと。

対するオンライン開催では、コミュニケーション手段は主に音声通話となりますが、同時に複数人が喋ると聞き取りが困難となるため、少なくともオフライン開催での「島」の単位でチャンネルを分ける必要があります。
しかしそうなると、チャンネルを超えてのビギナー同士での教え合いや情報共有は事実上不可能で、サポーターの負担が増大します。
筆者の経験上は、この形態ではサポーター1名につきビギナー2名程度までが限界で、サポーターをより多く用意する必要があります。
その一方で、新型コロナウイルス感染拡大が懸念される状況ではより衛生的であったり、遠隔地在住でリモート勤務の方が多い場合にも対応できるという利点があります。

今回は、ビギナーとして参加される方々が全員東京オフィス勤めであるとのことから、学習効率を高めることを重視して、東京オフィス内のラウンジスペースをお借りしてのオフライン開催としました。


### 実施結果

以上のような準備を行った上で、OSS Gateワークショップを実施しました。
[ワークショップの様子は参加された方の体験記をご参照いただく](https://developers.techouse.com/entry/OSS-Gate-Workshop-2024)として、ここからは、ワークショップの結果のまとめとなります。


#### ワークショップ自体の成果

今回、ビギナーとして参加されたのはTechouseさまの今年度新卒入社の方々でしたが、元々皆さん正式採用以前からインターンシップで実務に関わっておられたということもあり、GitHubの使い方など一般的なことで躓く部分はほとんどありませんでした。
そのためか、皆さん順調にワークショップの想定内容[^goal-of-uuorkshop]をこなされて、全員が1回以上のフィードバックを行っておられました。
進度が速かった方は、複数のフィードバックを行えた例もありました。

[^goal-of-uuorkshop]: 何らかのOSSをフィードバック対象として選定し、公式のドキュメントを見ながら使って見て、その過程で躓いた箇所について問題の報告や修正の要望としてフィードバックする、ということ。

時間的にも精神的にも余裕を持って取り組めたためか、当日はワークショップの作業の合間にサポーターへの（作業とは直結しないものも含めて）質問が多く行われていました。
筆者以外にサポーターとして参加した人曰く、RubyKaigiなどプログラミング言語ごとのイベントについてはTechouseさま社内でも情報が共有されていて認知しているものの、それ以外のイベントについてはあまり認知されていない様子だったことから、OSC（オープンソースカンファレンス）、Debian勉強会など、別の切り口のイベントも紹介したという一幕もあったそうです。


#### ワークショップの効果の客観的な測定

「効果測定」の項で述べたとおり、「ワークショップを通じて『やってみたら、それほど難しくなかった』『これなら、自分にも普通にできそう』という感想につながるような刺激を得て頂くこと」を実現できたかどうかを事前と事後のアンケート結果から測定・分析しました。

##### アンケートの内容

前後のアンケートは、「意識の変化」と「技術力の変化」のそれぞれを個別に測定するために、以下のように内容を設定しました。
実施後の方では、設問の立て方に違和感が生じるもののみ若干文面を調整しています。

* 実施前アンケート
  1. 以下の項目に関して、あなたの考えや経験に最も当てはまるもの1つにチェックを入れてください。（まったく無い、たまにある、頻繁にある の3段階で回答）
     （オープンソースに関する意識・認識の現状を知る事を意図した設問）
     * 使用するプログラミング言語やライブラリなどのソフトウェアがオープンソースかどうかを意識したことがある
     * OSSを使おうとしてつまずいたことがある　※自分のミスか、ソフトウェアの不具合か、ソフトウェアの機能不足かは問わない
     * OSSをREADMEやドキュメントの通りに使おうとして期待通りの結果にならなかったことがある　※自分のミスか、ソフトウェアの不具合か、ソフトウェアの機能不足かは問わない
     * 使用しているOSSのバグや不具合、ドキュメントのミスを直したいと思ったことがある
     * 使用しているOSSをもっと使いやすくしたいと思ったことがある
     * 自分が開発したコードをOSSとして公開したいと思ったことがある
  2. ライブラリやツールなどのソフトウェアが期待通りに動かない際、次のことをできそうですか？　あなたの考えに最も当てはまるもの1つにチェックを入れてください。（他の人の助けを得てもできなさそう、他の人の助けを得ながらできそう、自分で調べながらできそう、自信を持ってできそう の4段階で回答）
     （開発におけるコミュニケーション技術力とその認識の現状を知る事を意図した設問）
     * 自分が行った操作手順と、READMEやドキュメントに書かれている手順とを比較して、違う所がないかを確認することができる
     * GitHubのIssueを書き、開発者にバグや不具合を報告することができる
     * GitHub以外のWebサイト（GitLab.com、Bitbucekt.orgなど）でホスティングされているOSSにもフィードバックを行うことができる
     * 独自に手元で応急処置的な変更を施して使用することができる
     * 独自に手元で行った変更をPull Requestにして開発者に送ることができる
  3. OSSに限らず、バグや不具合などの情報を他の人に共有する際、次のことをできそうですか？　あなたの考えに最も当てはまるもの1つにチェックを入れてください。（他の人の助けを得てもできなさそう、他の人の助けを得ながらできそう、自分で調べながらできそう、自信を持ってできそう の4段階で回答）
     （開発における技術力・問題解決力とその認識の現状を知る事を意図した設問）
     * スタックトレースやコマンドの出力を省略せずに共有することができる
     * 再現手順と期待値を明確にして共有することができる
     * 自分の環境情報やソフトウェアのバージョンなどの前提情報を共有することができる
     * 必要に応じて画面のキャプチャや録画を取得し共有することができる
     * 説明を英語で書くことができる
* 実施後アンケート
  1. 以下の項目に関して、あなたの考えに最も当てはまるもの1つにチェックを入れてください。（まったく無い、たまにある、頻繁にある の3段階で回答）
     （オープンソースに関する意識・認識の現状を知る事を意図した設問）
     * 今後、使用するプログラミング言語やライブラリなどのソフトウェアがオープンソースかどうかを意識すると思う
     * ワークショップを通じて、過去OSSを使おうとしてつまずいたことがやっぱりあったと思う　※自分のミスか、ソフトウェアの不具合か、ソフトウェアの機能不足かは問わない
     * ワークショップを通じて、OSSをREADMEやドキュメントの通りに使おうとして期待通りの結果にならなかったことがやっぱりあったと思う　※自分のミスか、ソフトウェアの不具合か、ソフトウェアの機能不足かは問わない
     * 今後、使用しているOSSのバグや不具合、ドキュメントのミスを直したいと思う
     * 今後、使用しているOSSをもっと使いやすくしたいと思う
     * 今後、自分が開発したコードをOSSとして公開したいと思う
  2. ライブラリやツールなどのソフトウェアが期待通りに動かない際、次のことをできそうですか？　あなたの考えに最も当てはまるもの1つにチェックを入れてください。（他の人の助けを得てもできなさそう、他の人の助けを得ながらできそう、自分で調べながらできそう、自信を持ってできそう の4段階で回答）
     （開発におけるコミュニケーション技術力とその認識の現状を知る事を意図した設問）
     * （選択肢は実施前アンケートと同一）
  3. OSSに限らず、バグや不具合などの情報を他の人に共有する際、次のことをできそうですか？　あなたの考えに最も当てはまるもの1つにチェックを入れてください。（他の人の助けを得てもできなさそう、他の人の助けを得ながらできそう、自分で調べながらできそう、自信を持ってできそう の4段階で回答）
     （開発における技術力・問題解決力とその認識の現状を知る事を意図した設問）
     * （選択肢は実施前アンケートと同一）

前後どちらのアンケートも、設問1は「OSSに関する意識や理解」、設問2は「オープンソース活動で必要な技能」、設問3は「ソフトウェア開発一般で必要な技能」を問うことを意図した内容になっています。


##### アンケートの結果

実際のアンケート結果を集計したグラフは以下の通りです。  
![（グラフ：設問1の回答の変化。事前段階では「まったくない」「たまにある」がほとんどであった回答が、事後では「頻繁にある」の回答が大きく増加している。）]({% link /images/blog/techouse-oss-gate-workshop-report/q1.png %})  
![（グラフ：設問2の回答の変化。事前段階では「他の人の助けを得ながらできそう」「他の人の助けを得てもできなそう」という回答が少なくないが、事後では「他の人の助けを得てもできなそう」がなくなり、「自信を持ってできそう」「自分で調べながらできそう」が増加している。）]({% link /images/blog/techouse-oss-gate-workshop-report/q2.png %})  
![（グラフ：設問3の回答の変化。前後で有意な傾向の変化は見られない。）]({% link /images/blog/techouse-oss-gate-workshop-report/q3.png %})  
全体としては、設問1と設問2の結果は知識・理解・技能の向上が見られ、設問3の結果は特に変化は見られなかったと言えます。
以下、各設問について詳しく述べます。

設問1の結果からは、参加者の皆さんに「OSSに関する意識や理解が全体的に高まった」と感じて頂けた様子が窺えます。
今回は厳密な効果測定のためのペーパーテスト等は実施しておらず、ここではあくまで主観的な印象の変化しか把握できていないことには注意が必要ですが、少なくとも、実際にオープンソース活動に取り組む上で「何も分からない」混乱した状態で放り出すことにはなっていなかった、とは言えそうです。

設問2の結果からは、「オープンソース活動で必要な技能も、ワークショップ実施前よりも高まった」と感じて頂けた様子が窺えます。
ただ、これについては「すでに身に着いていた技能をオープンソース活動にも応用可能だと気が付いたから」と解釈するのが妥当かも知れません。
というのも、アンケートの設問として挙がっている話題は実際のところ、*現代のソフトウェア開発における開発者同士のコミュニケーションや開発の進め方に関わる部分*ですが、これらは元々、チームや企業によって文化の違いが大きくなりがちです。
今回のワークショップの体験は、*今まで自社の開発プロジェクトでのみ作業をしていたところから、他の会社の開発チームとも作業をしてみた*状況に似ていて、元々の技術力に大きな優劣の差が無いのであれば、問題になるのはチューニングの部分です。
サポートした所感としては、限られた時間の中で新しく伝えた知識はそれほど多くなく、ほとんどのことは「自社プロジェクト内で、自社の他の開発者の人とやり取りする」上で経験済みだったようで、ほぼ説明なしでやって頂けた感覚があります。
それを考慮すると、設問2の回答の変化は、*既に知っていた・やればできる状態だったことについて、「この程度では、できるとは言えない」と悲観的に考えていたのが、実際の活動を通じて「これでも『できる』と言っていいんだ」と認識が改まった*ということであろうと考えられます。

その一方で、設問3の結果からは、不具合の原因調査や修正における一般的なスキルについてはワークショップの前後でほぼ変化が無かった様子が窺えます。
Techouseさまのように、すでにオープンソースのライブラリーやフレームワークを使用して自社サービスを開発している状況では、オープンソース開発プロジェクト本体で使う技術もほぼ同じです。
元々自分のスキルレベルを的確に把握できていたのであれば、普段の業務とほぼ変わらないことをしたということで、この結果は妥当と言えるでしょう。


#### オープンディスカッション

前述したとおり、ワークショップの最後には「Techouseさま社内でオープンソース活動を推進していくにはどうすればよいか？」をテーマにオープンディスカッションを行いました。
この時間で参加者の方々から出た話題を紹介します。  
![（プロジェクターで知見を共有しながらふりかえりを行う様子の写真）]({% link /images/blog/techouse-oss-gate-workshop-report/03.jpg %})

##### 業務とオープンソース活動の両立：時間のかけ方

「自社のビジネスに直接的には寄与しない、OSSのライブラリーのバグ調査などに腰を据えて取り組むのは、難しい気がする。プロジェクト中にオープンソース活動をするビジョンが思い浮かばない。」
ビギナー参加者の方からこのような声が上がり、他の方も同様のことを感じている様子であったことが窺えました。
同様の疑問・悩みは他社での実施事例でも伺っており、普遍性のある話題と言えそうです。

修正のプルリクエストを行うような場合はそれなりに時間がかかるのは致し方ないですが、ドキュメントや表示メッセージの誤記の報告、あるいは不具合であっても原因究明にまでは踏み込まない場合であれば、「30分ほど作業の手を止めて調べ物をする」ような感覚でフィードバックを行えるはず、というのが筆者の認識です。

ただ、比較的簡単な内容であっても、慣れないうちほど時間がかかる傾向はあります。
過去のワークショップ参加者の様子から、これは、「こういうフォーマットに沿って報告すればよいのだ」という要領が掴めていなかったり、失礼がないようにと気を遣って報告文を考えるのに時間がかかったり、ということが原因である場合が多いようです。
これは、回数・経験を重ねる中での慣れによって解決されていくのを待つしかない問題でしょう。
筆者としては、そのようにして無理なくできる範囲の活動を繰り返して自信を付けた上で、難易度の高い課題に取り組む機会も少しずつ設けていくとよいのではないかと思っており、そのように伝えてみました[^progressive]。

[^progressive]: 例えば、「30分から1時間程度かけて取り組んでみて、できなければ今は諦める（より経験がある人に後を任せる、あるいは、将来自分のスキルが増したときに改めて取り組む）」といった要領で、時間ベースで区切るやり方が考えられます。これなら、「業務が忙しいのに、深入りしすぎて丸1日を消費してしまった」というような事態を回避しやすいのではないでしょうか。

##### 業務とオープンソース活動の両立：挑戦の機会としての意義

前述の話題について、Techouseさまのシニアエンジニアの方からは、
「筋のいい解決を試みるには時間がかかる。しかし、それを避け続けていると、技術力が先細りになる。経営層などの理解を得られるようシニア層が掛け合っていくので、皆はプロジェクト遅延を過度に恐れずに、筋のいい解決に是非取り組んでほしい」
「プロジェクト内ではできなくても、後で別途時間を取ったり、先輩を巻き込んだりして取り組んでほしい」
といった趣旨のコメントも頂けました。

ライブラリーを使用していて問題が起こったとき、「とりあえずエラーを握り潰そう」「原因がよく分からないので、とりあえず迂回しよう」あるいは「別のライブラリーに乗り換えてしまおう」といった要領で、なるべく踏み込まずに解決を図ろうとする人は少なくないでしょう。
こういった「小手先の解決」は、その場で手早く話を終わらせることはできるのですが、プロジェクトの継続性に負の影響を色々と及ぼしがちです。
全く異なる思想のライブラリーに無理矢理入れ換えるために全体の設計が歪になったり、後々何かのきっかけで不具合が再発してしまったり、障害発生時の調査が却って困難になったりと、無用なリスク要因を増やす結果になりやすく、そのため「筋が悪い」と見なす人は少なくありません。

ブラックボックスであることが多いプロプライエタリ[^proprietary]なソフトウェアを相手にする場合、こういった結論に至らざるを得ないことはあります。
しかし、OSSではそんなことはありません[^source-available]。

[^proprietary]: 内部の詳細が非公表だったり、知的財産権によって厳重に保護されていたりしている、ユーザー側の様々な権利が制限されているソフトウェア。商用ソフトウェアは商売の都合のためにプロプライエタリになりがちです。
[^source-available]: Source Availableでも、一定の条件のもとでソースコードを調査することは可能なので、厳密に言えば、オープンソースに限った話ではありません。

1. まず、エラー情報やライブラリーのコードを元に調査し、トラブルの原因を特定する。
2. 場合によっては、そのライブラリー自体の変更履歴を調査して、トラブルの原因が生じた経緯を把握する。
3. 経緯を踏まえた適切な解決方法を考える。
   * ライブラリーの不具合であれば、不具合としてissueを報告する。
   * アプリケーション側で対応する必要があるのなら、アプリケーションを改修する。

といった具合に、問題の根本原因まで調べ上げた上で、リスク要因を減らす「筋の良い」解決策を採用できる道が開かれています。
この過程で得られる経験の価値は計り知れず、だからこそ、我々やTechouseさまのシニアエンジニアの方々は、「筋の良い」解決を試みることを若手に推奨していると言えます。

開発プロジェクトへの不具合報告を行うために必要になる調査や、プルリクエスト作成のために必要になる作業を、「自分達のプロジェクトとは無関係の物へのコミットメント」と捉えると、確かに、そこに時間をかけることは憚られるでしょう。
しかし、「自分達のプロジェクトとして適切な解決方法を探るために必要な調査・修正の作業」と捉えれば、ある程度の時間をかけることにも正当性を認めやすくなります。

Techouseさまが会社としてそのように良いやり方を許容・推奨していても、こういった文化的なことは、案件の状況次第ではなかなか伝えにくいものです。
今回のディスカッションは、Techouseさまの良い開発者文化を、改めて新人の方々に伝える機会にしていただけたと言えるでしょう。


##### オープンソース活動の「楽しさ」について

「これまで、『プライベートな時間にまで仕事と同じことをする（コードを書くなど）楽しさ』が分からなかった。
しかし、今回のワークショップで行ったフィードバックでは、楽しさを感じた。」
ワークショップに参加された方からは、このようなコメントも頂きました。

OSS Gateワークショップでは、「実際に公開のソフトウェアを使ってみて、つまずいた所をissueとして報告する（その上で、可能であれば修正に協力する）」やり方でオープンソース活動を体験してもらっています。
これは、「現実の問題の発見から原因究明、解決までの一通りのことを主体的にやる」体験と言えます。
今回ワークショップに参加された方々は、インターンでの実務経験があるとはいえ新卒1年目で、業務上では、分割済みのタスクを上長の方の指示に従ってこなす場面がまだ多かったのだと思われ、普段の業務とは離れた場でのこととはいえ、このような体験は良い刺激となった様子が窺えました。

自社で製品を生産している会社では、新卒研修の一環として、製品ができるまでの一通りの流れを体験してもらうことがあると聞きます。
Webサービスを運用する企業でそれに類することをするとしたら、「1つの機能の企画から実装までを行う」といったやり方が考えられますが、「ほどよい」難易度の課題を毎回都合良く用意できるとは限りません。
オープンソース開発プロジェクトを題材にしたフィードバックは、そのような効果を期待して行う研修としてちょうどいいのではないか、と筆者は考えています。




### まとめ

以上、Techouseさまのオープンソース開発支援活動における新人研修の一環としてのOSS Gateワークショップ実施事例について、背景事情と、状況に合わせてカスタマイズした部分、および実施の結果がもたらした効果の考察をご紹介しました。
この度オープンソース活動のサポートを行うにあたり、Techouseさまには企画段階での打ち合わせに始まり、実施当日のサポーターとしてのご参加、ワークショップ実施前後のアンケートやその後の追跡調査など、各段階で多大なご協力を頂きましたことに改めてお礼を申し上げます。

OSS Gateワークショップは「実際のOSSを触り、フィードバックできそうな内容を見付けて、オープンソース開発プロジェクトにフィードバックする」という内容です。
やることの範囲が広いため、どこに着目するかによって得られる物が変わってきますし、何を得たいかによってどこに注力するかが変わってきます。
今回は、元々のワークショップの内容がTechouseさまの状況に非常にマッチする内容だったこともあって、「『やってみたら、それほど難しくなかった』『これなら、自分にも普通にできそう』という感想につながるような刺激を得てもらう」という目標に無理なく到達することができました。

また、実施時のサポーターとして先方のシニアエンジニアの方にご参加頂いたことで、普段の業務とは別の切り口での密なコミュニケーションの機会となり、周辺的な技術知識や問題解決への取り組み方・その心構えなど、Techouseさまのシニアエンジニア層の間では共有されていつつも新人の方々へは充分に伝え切れていなかった大事な情報を、改めて伝えて頂くこともできました。
筋の良い解決の価値を重んじ、そのための社内学習にも力を入れておられるTechouseさまにおいて、良い文化を育てることの一助を担うことができ、当社としても嬉しい限りです。

株式会社クリアコードは、お客さまの状況に最適な支援の仕方をお客さまと一緒に考えて実施するサポートを、有償にてご提供しています。
オープンソース開発へ関わりたいとお考えの企業や、目的達成のための手段としてオープンソース開発への関わりを検討されている企業のご担当者さまは、是非とも[お問い合わせフォーム]({% link contact/index.md %})よりご連絡下さい。


