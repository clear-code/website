---
title: Debianパッケージのメンテナンスをsalsa.debian.orgに移行するには
tags:
  - debian
---
### はじめに

これまでDebian関連の開発プラットフォームとしては、[Alioth](https://alioth.debian.org/)と呼ばれる[FusionForge](https://fusionforge.org/)を使ったサービスが提供されていました。[^0]
最近では、Aliothの後継として[Salsa](https://salsa.debian.org/)と呼ばれるGitLabを使ったサービスへの移行が進んでいます。[^1]
<!--more-->


今回は、メンテナンスしているパッケージをgit-buildpackage(gbp)のやりかたに合わせつつsalsa.d.oへと移行する方法を紹介します。

### salsa.d.oへなぜ移行するのか

[libhinawa](https://github.com/takaswie/libhinawa)を例に紹介します。libhinawaパッケージのメンテナンスでは、これまでアップストリームがdebianディレクトリをあらかじめ用意してくれていたので、そちらに都度フィードバックする方法で実施していました。

このままGitHubのリポジトリでメンテナンスし続けてもよいのですが、アップストリームの側ではソフトウェアそのものの開発に集中してもらおうということで、salsa.d.oへの移行を[提案](https://github.com/takaswie/libhinawa/issues/54)したところ、libhinawaの開発者である [@takaswie](https://twitter.com/takaswie) さんから承諾を得たので移行に踏み切ることにしました。

アップストリームにとってみれば、debian/*以下をメンテナンスする必要がないので負担が減らせるのと、salsa.d.oにリポジトリを置くことで、他のDebian開発者からのフィードバックを期待できます。 [^2]

従来のやりかたに比べるとリポジトリが分散してしまうという点や、マージリクエストを送りたい人はsalsa.d.oのアカウントが必要になるので気軽にフィードバックしにくくなるかもしれないという懸念はありますが、いまのところフィードバックする人も限られているので、そのあたりは許容できるものと判断しました。

### gbpを使ったメンテナンスのやりかたを決める

salsa.d.oに移行するにしても、メンテナンスのワークフローは特に定めていませんでした。
そこで、salsa.d.o移行を機に、メンテナンスのやりかたを整備することにしました。そこで採用したのが[gbpを使ったワークフロー](https://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.workflow.html)です。[^3]
まずはgbpを使ってメンテナンスをはじめてみることにしました。

### gbpの設定を行う

最初に gbp を使うための設定ファイルを `~/.gbp.conf` に作成しました。その際、以下の方針にしました。

  * wiki.d.oにある[設定内容](https://wiki.debian.org/PackagingWithGit#Packaging_with_Git)をベースにする

  * ブランチはmasterではなくdebian/unstableにする

  * ビルド方法にはpbuilder+tmpfsを利用する構成を前提とする

  * ビルド後にlintianで詳細なログを出しつつパッケージのチェックをする

このような方針でカスタマイズした設定は以下の通りです。

```
# See https://wiki.debian.org/PackagingWithGit#Packaging_with_Git

[DEFAULT]
builder = DIST=sid ARCH=amd64 GIT_PBUILDER_AUTOCONF=/etc/pbuilderrc BUILDER=pbuilder git-pbuilder
cleaner = fakeroot debian/rules clean
# Create pristine-tar on import
pristine-tar = True
# Run lintian to check package after build
postbuild = lintian -EviIL +pedantic $GBP_CHANGES_FILE

[buildpackage]
export-dir = ../build-area/
debian-branch = debian/unstable

[import-orig]
# Filter out unwanted files/dirs from upstream
filter = [
    '*egg.info',
    '.bzr',
    '.hg',
    '.hgtags',
    '.svn',
    'CVS',
    '*/debian/*',
    'debian/*'
    ]
# filter the files out of the tarball passed to pristine-tar
filter-pristine-tar = True

[import-dsc]
filter = [
    'CVS',
    '.cvsignore',
    '.hg',
    '.hgignore',
    '.bzr',
    '.bzrignore',
    '.gitignore'
    ]

[dch]
# ignore merge commit messages
git-log = --no-merges
```


pbuilderに関しては[Ubuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するには]({% post_url 2014-11-21-index %})で言及しているPbuilderTricksと[Pbuilder と Cowbuilder の性能比較]({% post_url 2015-01-15-index %})で言及している `APTCACHEHARDLINK=no` を `/etc/pbuilderrc` に反映しました。

### gbp向けにインポートして作業用のリポジトリを作成する

gbp向けの設定ファイルの準備ができたので、次に作業用のリポジトリを準備しました。
やりかたはいろいろあるようですが、今回はリリース済みのバージョンをもとに作業用のリポジトリを作成することにしました。

今後作業ブランチが増えることも考慮して、 `master` ブランチではなく `debian/unstable` ブランチで作業します。
そのようにするためには、インポートする際に `--debian-branch` オプションで明示的に `debian/unstable` ブランチの指定が必要です。

インポートするためには `gbp import-dsc` を使って行います。[^4]

```
% gbp import-dsc --debian-branch debian/unstable --pristine-tar http://http.debian.net/debian/pool/main/libh/libhinawa/libhinawa_0.8.2-1.dsc
```


このようにすることで、ブランチが3つ作成されます。

```
% git branch -a
* debian/unstable
  pristine-tar
  upstream
```


通常パッケージのメンテナンスで使うのはインポート時に明示的に指定した `debian/unstable` ブランチです。`pristine-tar` ブランチには `tar.gz` への差分が含まれています。`upstream` ブランチには `debian/*` を含まないソースコードがインポートされます。`pristine-tar` や `upstream` ブランチへは `gbp import-orig` などのコマンドを実行したときに自動的にコミットされるので、明示的にコミットする必要はありません。

### gbpでパッケージをビルドする

作業用のブランチができると、以下のコマンドでパッケージをクリーンビルドできるようになります。

```
% gbp buildpackage
```


ビルド後にはlintianによるパッケージのチェック結果が表示されるので、報告された問題を都度修正するのがよいでしょう。

### 作業リポジトリをsalsa.d.oにインポート

ここまででパッケージの体裁を整えることができたら、次は作業リポジトリを実際にsalsa.d.oにインポートします。
ただし、salsa.d.o/debian配下にインポート作業ができるのはDebian開発者だけなので、権限がない場合には依頼しましょう。`libhinawa` の場合は [@henrich](https://twitter.com/henrich) さんにお願いしました。

インポートしてもらった結果、https://salsa.debian.org/debian/libhinawa が今後の作業リポジトリとなりました。

(2018年03月28日追記：salsa.d.o/(ユーザー名)配下にある個人のプライベートリポジトリには自由にインポートできます。そのため作業を依頼する必要がでてくるのは「salsa.d.o/debian/配下に」の場合であることを明記しました。)

### gbpでタグを打つ

リポジトリの移行後、debian/controlの `Vcs-*` の変更などが必要だったので、新しいバージョンとしてlibhinawa 0.8.2-2を出すことにしました。
mentors.d.nへアップロードしたパッケージが無事unstableに入ったら、対応するタグを打っておきます。[^5]

タグを打つには、以下のコマンドを実行します。

```
% gbp buildpackage --git-tag --git-debian-branch=debian/unstable
```


ここまでで、ひととおりのリリースまでの作業が完了です。

### まとめ

今回は、メンテナンスしているパッケージをgit-buildpackage(gbp)のやりかたに合わせつつsalsa.d.oへと移行する方法を紹介しました。

GitHubのインターフェースに慣れているとAliothはとっつきにくかったかもしれませんが、salsa.d.oはGitLabなのでよりフィードバックしやすいかもしれません。

[^0]: AliothはSourceForge.netやOSDN.net、savannah.gnu.orgのようなものです。

[^1]: salsa.d.oはあくまで選択肢の一つなので、必ず使わなければならないというわけではありません。

[^2]: Debian開発者はsalsa.d.o/debian/以下に作成されたリポジトリへコミット権を持っていたりします。

[^3]: Gitを使ったワークフローとしては昔からあるので、おそらく定番のはず。

[^4]: tar.gzをインポートする場合にはgbp import-origを代わりに使います。

[^5]: もしかしたらrejectされてアップロードしなおすことになるかもしれないのでそのようにしている。
