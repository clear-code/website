---
title: LTS版 Fluent Package v5.0.4をリリース
author: fujita
tags:
  - fluentd
---
こんにちは。[Fluentd](http://www.fluentd.org)チームの藤田です。

2024年7月2日にFluent PackageのLTSの最新版となるv5.0.4をリリースしました。

本リリースでは、パッケージでいくつかの修正と改善が行われています。

本記事ではFluent Packageの変更内容について紹介します。

<!--more-->

### Fluent Package v5.0.4

2024年7月2日にFluent PackageのLTSの最新版となるv5.0.4をリリースしました。
同梱のFluentdのバージョンはv1.16.5から変わりありません。

本記事ではFluent Packageの変更内容について紹介します。
興味のある方は、以下の公式情報もご覧ください。

公式リリースアナウンス

* Fluent Package v5.0.4: https://www.fluentd.org/blog/fluent-package-v5.0.4-has-been-released

CHANGELOG

* Fluent Package v5.0.4: https://github.com/fluent/fluent-package-builder/releases/tag/v5.0.4

### 修正

Fluent Package v5.0.4では、以下の修正をしています。

* Windows: fluent-gem(td-agent-gem)コマンドがPATH環境変数を誤って更新してしまう問題を修正

#### Windows: fluent-gem(td-agent-gem)コマンドがPATH環境変数を誤って更新してしまう問題を修正

限定的なケースでコマンドの動作に影響を及ぼす不具合を修正しました。

1. `fluent-gem`(`td-agent-gem`)コマンドを実行する
2. その後同じシェル上で`fluentd`(`td-agent`)コマンドを実行する

上記のようにコマンドを実行した場合に、`fluent-gem`(`td-agent-gem`)コマンド実行時にシェル実行環境におけるPATH環境変数が意図せず更新されてしまい、
後続の`fluentd`(`td-agent`)コマンド実行時に想定とは異なるパス上の`fluentd.bat`が実行される問題が発生していました。

この問題は`fluent-gem`(`td-agent-gem`)と`fluentd`(`td-agent`)を別々のシェルでの実行により回避することもできます。

### 改善

Fluent Package v5.0.4では、以下の改善をしています。

* Fluentdのサービスが起動中は新規Fluentdの重複起動を防止

#### Fluentdのサービスが起動中は新規Fluentdの重複起動を防止

Fluent Packageをインストールすると、Fluentdサービスが起動します。
それとは別にシェル上から`fluentd`(`td-agent`)を実行すると、これまではデフォルトの設定ファイルパスでFluentdを起動することができました。

Fluentdのバージョンを確認したい場合には`fluentd --version`(`td-agent --version`)と実行する必要があるのですが、
誤って`fluentd -v`(`td-agent -v`)を実行すると、ログが詳細に表示されるモードでFluentdが起動してしまいました。

このように意図せずFluentdを多重起動してしまうとバッファファイルなどを破損させてしまう恐れがありました。

Fluentdサービス起動中にシェル上から`fluentd`(`td-agent`)コマンドで起動する場合は、`--config`オプションで設定ファイルパスを明示しない限りは、
サービスと重複して起動しないように改善しました。

`--dry-run`オプションのように、Fluentdを起動する用途以外でのコマンド実行はこれまでどおりサービスの稼働状況とは関係なくできます。

より安全に利用していただけるようになりました。

### その他の修正・改善

* Ubuntu 24.04 (Noble Numbat)をサポート対象に追加
* Rubyのバージョンを3.2.4にアップデート
  * Ruby 3.2.4にはセキュリティアップデートがいくつか含まれています。詳細は[Ruby 3.2.4 リリース](https://www.ruby-lang.org/ja/news/2024/04/23/ruby-3-2-4-released/)をご覧ください。

### まとめ

今回は、Fluentdの最新パッケージFluent Package v5.0.4の情報をお届けしました。

パッケージでいくつかの修正と改善が行われました。
最新版のFluent Packageへのアップデートを推奨します。

長期の安定運用がしやすいLTS版をぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
