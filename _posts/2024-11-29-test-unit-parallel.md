---
tags:
- ruby
title: test-unitで並列テスト実行
author: kou
---
[test-unit](https://github.com/test-unit/test-unit)をメンテナンスしている須藤です。最近[@tikkss](https://github.com/tikkss/)とtest-unitを改良しているので紹介します。テストを並列実行できるように改良しています。

<!--more-->

### きっかけ

@tikkssは[Red Data Tools](https://red-data-tools.github.io/ja/)に参加しているのですが、[red-datasetsのテストが遅いのをどうにかする](https://github.com/red-data-tools/red-datasets/issues/188)ということに取り組んでいました。いくつかのケースはテスト内容を減らすことで高速化しましたが、その方法で高速化できるものは一通り対応しつくしました。これ以上の高速化はtest-unitでテストを並列実行できるようにするしかないのでは！？という気持ちになったので@tikkssと一緒に[取り組み始め](https://github.com/test-unit/test-unit/issues/235)ました。

週に1回、お昼の30分で一緒に取り組んでいます。最近は毎週水曜日の12:15-12:45に取り組んでいます。私は東京に住んでいて@tikkssは島根に住んでいるので、[Jitsi Meet](https://meet.jit.si/)を使ってオンラインで取り組んでいます。取り組んでいる様子はYouTubeでライブ配信しています。[アーカイブ](https://www.youtube.com/playlist?list=PLKb0MEIU7gvSFaWSGEVOPqyvp8v5PCTYt)もしているので、あとから視聴することもできます。

### 予定

次の3種類の並列実行をサポートする予定です。

* `Thread`ベース
* マルチプロセスベース
* `Ractor`ベース

### 現状

実はまだ完成していないのですが、一部動くものはできています。`Thread`を使った並列実行機能は動いていて、マルチプロセスや`Ractor`を使った並列実行機能はまだ動いていません。この間リリースした[test-unit 3.6.3](https://github.com/test-unit/test-unit/releases/tag/3.6.3)に`Thread`を使った並列実行機能は入っているので、興味がある人は`--parallel`オプションを使ってみてください。

なお、普通は`Thread`を使って並列実行してもテストは速くなりません！Rubyで`Thread`を使って速くなるのはIO待ちが多いときです。Rubyの`Thread`は同時にRubyのコードを実行できないからです。IO待ちになると他の`Thread`に処理が移るのでIOを待っている間も違う処理をできます。待ち時間を有効活用できるのでIO待ちがある場合は速くなるというわけです。

普通はテスト中にIO待ちになることはそんなに多くありません。そのため`Thread`を使った並列実行機能で速くなることはほぼないんですねぇ。むしろ、オーバーヘッドがある分、微妙に遅くなります。

そんなに速度向上を期待できないのにどうして実装したかというと、練習のためです。まずは一番簡単な`Thread`ベースで汎用的な並列実行の仕組みを用意して、その上でマルチプロセスベース・`Ractor`ベースの仕組みを実装していこうとしています。

### 今後

`Thread`ベースの仕組みが動くようになったので、マルチプロセスベースの実装に着手し始めています。まだ、各プロセス間でどのように通信するかも決まっていません。今後、プロセス間の通信プロトコルや、実行するテストの情報をどうやって共有するか、テスト結果をどう収集するかなどを検討して実装していくことになります。

マルチプロセスベースの仕組みが仕上がったら`Ractor`ベースの仕組みに着手する予定です。

マルチプロセスベースの仕組みは速くなると思いますが、`Ractor`ベースの仕組みは速くなるかどうかはやってみないとわからないなぁと思っています。

また、こまごました使い勝手の部分の作り込みもまだなのでそのあたりも整備していかないといけません。たとえば、各ワーカーにワーカーIDのような情報を渡してあげないといけません。並列実行する場合、各ワーカーが別々のリソース（たとえば、Railsアプリケーションならデータベースとか）を使った方が速度がでます。共有リソースをロックしたりする必要がなくなるからです。ワーカーIDのような情報があると各ワーカーごとに独立したリソースを確保しやすくなります。

### まとめ

最近、@tikkssと一緒に取り組んでいるtest-unitの並列実行対応について紹介しました。これまでどうやって設計・実装してきたかが気になる人は[アーカイブ](https://www.youtube.com/playlist?list=PLKb0MEIU7gvSFaWSGEVOPqyvp8v5PCTYt)や[test-unit/test-unit#235](https://github.com/test-unit/test-unit/issues/235)に残されている@tikkssのメモを参照してください。一緒に開発したい人は[Red Data Toolsのチャット](https://app.element.io/#/room/#red-data-tools_ja:gitter.im)に来てください。

