---
tags:
  - apache-arrow
  - ruby
  - presentation
title: 'DataScience.rb ワークショップ 〜ここまでできる Rubyでデータサイエンス〜：RubyもApache Arrowでデータ処理言語の仲間入り
  #datasciencerb'
---
須藤です。最近気になるApache Arrowのissueは[[ARROW-1055] [C++] Create add-on library for CUDA / GPU integration](https://issues.apache.org/jira/browse/ARROW-1055)です。
<!--more-->


2017年5月19日に開催された[DataScience.rb ワークショップ 〜ここまでできる Rubyでデータサイエンス〜](https://rubyassociation.doorkeeper.jp/events/59965)で「RubyもApache Arrowでデータ処理言語の仲間入り」という話をしました。このイベントはしまねソフト研究開発センタースポンサーのプロジェクトとRubyアソシエーションスポンサーのプロジェクトの成果発表会のような位置付けだと思うのですが、私がApache Arrowの紹介をしたいと言ったので、それらとまったく関係のないApache Arrowの話をねじ込んでもらいました。ありがとうございます。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/data-science-rb/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/data-science-rb/" title="RubyもApache Arrowでデータ処理言語の仲間入り">RubyもApache Arrowでデータ処理言語の仲間入り</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/data-science-rb/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/datasciencerb)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-data-science-rb)

### 内容

Ruby界隈では[Apache Arrow](https://arrow.apache.org/)のことを知っている人はあまりいないはずです。その前提で、私の発表ではApache Arrowがなにを目指しているプロジェクトで、現状はどうなっていて、今後はどうなる予定か、ということを紹介しました。

詳細はスライドを参照してもらうとして、ざっくりいうと次のような感じです。

  * Apache Arrowが目指していること：

    * 低いデータ交換コスト

    * 最適化されたデータ処理実装の共有

  * Apache Arrowの現状：

    * 「低いデータ交換コスト」の方はプロダクションで使えそうなくらいになっている

    * 「最適化されたデータ処理実装の共有」の方はこれから

    * Java、C++、Python、Ruby、Lua、Go、JavaScript間で低コストでデータ交換できる

  * Apache Arrowの今後：

    * Apache Arrowを使ったデータ交換の推進（たとえば、[SparkとPySpark間でのデータ交換にApache Arrowを使う変更](https://github.com/apache/spark/pull/15821)がマージされて現実に使っていく。似たようなことをいろいろなプロジェクトで進めていく。）

    * 最適化されたデータ処理実装の提供

    * Apache Arrow対応言語の増加

どうしてRubyがApache Arrowに対応するとデータ処理できるようになるかを説明するために、まず、Apache Arrowがでてきた背景を説明します。

現在の（ビッグデータの）データ分析システムは1つの大きなソフトウェアでなんでもかんでも全部やるというやり方ではなく、複数のシステムが協調してデータ分析するというやり方になっています。たとえば、データの管理はHadoopでデータの分散処理基盤はSparkでユーザーがカスタマイズする部分はPythonで、といった具合です。

複数のシステムが協調するためには分析対象のデータを相互にやりとりする必要があります。データが大量にあるのでデータ交換のコストがどのくらいかは重要です。コストが高いと本来やりたかったデータ分析処理に時間を使えずにデータ交換ばかりに時間を使ってしまうからです。現状はコストが高くてそのような状況になりがちでした。それを解決するためにでてきたのがApache Arrowです。

Apache Arrowはデータのシリアライズ・デシリアライズコストをほぼ0にします。そのため、データ交換のコストの多くはデータの送受信くらいになります。別マシン間でのやりとりであればネットワーク上での通信処理、同一マシン間でのやり取りであればメモリー上へのデータの読み書き処理が主なコストになるので、かなり高速です。

データ交換コストが低くなると、データ分析システムの一部にRubyを使いやすくなります。これまでは、データ交換コストが高いので、できるだけサブシステム間でのデータ交換を減らしたくなりましたが、コストが低いとカジュアルにデータ交換できます。そうすると、「この処理はRubyが得意だからこの処理だけRubyでやろう」ということがしやすくなります。Rubyでできることが増えていくともっとRubyの活躍の場が増えます。

RubyがApache Arrowに対応するとRubyにデータが回ってくるようになるのでデータ処理できるようになるということです。

というのが私の目論見ですが、このようになるといいなぁと思ってRubyでApache Arrowを使えるようにする作業を進めています。現在の状況は次の通りです。

  * Apache ArrowはRubyを公式サポート

    * 私が開発した成果はApache Arrow本家に取り込まれました。継続的な活動を認められてコミッターにもなりました。

  * RubyからApache Parquet・Apache Arrow・Feather形式のデータを読み書き可能

    * Apache ParquetはHadoop界隈でよく使われている形式です。Hdoop方面のデータを読み書きできるということです。Apache ArrowのC++実装とApache ParquetのC++実装が連携しているので、それらの成果を利用しています。

    * FeatherはR界隈でよく使われている形式です。Rで処理したデータをRubyで読み込んだり、Rubyで収集・加工したデータをRに渡したりできます。

  * Apache Parquet・Apache Arrow・Feather形式のデータとRuby/GSL、NMatrix、Numo::NArrayのデータを相互変換可能

    * Ruby/GSL、NMatrix、Numo::NArrayはRuby用の行列演算ライブラリーです。読み込んだデータをこれらのライブラリーのオブジェクトに変換すればそれらの機能でデータを処理できます。

    * サンプルコードは[kou/rabbit-slide-kou-data-science-rb/sample/](https://github.com/kou/rabbit-slide-kou-data-science-rb/tree/master/sample)にあります。

  * Apache Arrow形式のデータと[Groonga](http://groonga.org/ja/)のデータを相互変換可能

    * Groongaは集計機能も得意な全文検索ライブラリーです。RubyにはよくできたGroongaのバインディングがあるので、Groongaを使って高速にデータ処理することができます。たとえば、ログメッセージを全文検索して対象のログを絞り込んだり、全文検索インデックス内の統計情報を活用して重要語のみを抽出したりできます。

    * GroongaはApache Arrowに対応している（Groonga自体にApache Arrow形式のデータを読み書きする機能がある）ので、Rubyを経由せずに相互変換できて高速です。

今後ですが、1人でやっていては限界があるので、[Red Data Toolsプロジェクト](https://red-data-tools.github.io/ja/)として「Rubyでデータ処理したい！」という人たちと一緒に活動していく予定です。このプロジェクトは次のようなポリシーで活動していくので、賛同できる人はぜひ一緒にRubyでデータ処理できるようにしていきましょう！参加する人は[チャット](https://gitter.im/red-data-tools/ja)で参加表明してください。なにから着手するか相談しましょう。

> [["  1. Rubyコミュニティーを超えて協力する", [["     * 私たちはRubyコミュニティーとも他のコミュニティーとも協力します。たとえば、私たちは多くの言語が共通で使っているApache Arrowを使いますし、Apache Arrowの開発に参加して開発成果を共有します。"]]], ["  1. 非難することよりも手を動かすことが大事", [["     * 私たちは現状（RubyよりもPythonの方がたくさんよいツールが揃っているかもしれません）や既存ライブラリーの実装を非難することなどに時間を使うよりも、コードを書いたりテストをしたりドキュメントを書いたり私たちの活動を紹介したり他のプロジェクトにフィードバックをしたりといったことに時間を使います。"]]], ["  1. 一回だけの活発な活動よりも小さくてもいいので継続的に活動することが大事", [["     * Rubyでたくさんのデータ処理をできるようになるために私たちはたくさんやることがあるかもしれません。データ処理のためのすばらしいツール群一式を揃えるために継続的に活動する必要があります。そのため、1回だけの活発な活動よりも継続的な活動の方が大事です。"]]], ["  1. 現時点での知識不足は問題ではない", [["     * 私たちは高速なツールを実装するために数学や統計学や線形代数学などの知識が必要かもしれません。しかし、このプロジェクトに参加する時点でそれらの知識は必須ではありません。なぜなら活動をしていく中で学んでいくことができるからです。私たちは既存の高速な実装を使ったり、既存の高速な実装から学んだりすることができます。"]]], ["  1. 部外者からの非難は気にしない", [["     * 私たちがデータ処理のためのすばらしいツール群一式を揃えるまでに時間がかかるかもしれません。そうなるまでは部外者が私たちの活動を非難するかもしれません。私たちはそれらを気にしません。私たちにはそれらを処理している時間はありません。 :-)"]]], ["  1. 楽しくやろう！", [["     * Rubyを使っているんですから！"]]]]


### まとめ

Rubyでデータ処理できるようになるために役立ちそうなApache Arrowの紹介と現在できることを紹介しました。また、これからさらにRubyでデータ処理できる状況を推進していくために、Red Data Toolsプロジェクトを立ち上げました。Red Data Toolsプロジェクトに参加する人は[チャット](https://gitter.im/red-data-tools/ja)で参加表明してください。

「Rubyでデータ処理できるようにしよう！」活動を推進するために、開発費を提供するという方法もあります。手は動かせないけどお金は出せるという方は[ご連絡](/contact/?type=ruby)ください。

Apache Arrowは去年のはじめに始まったばかりの新しいプロジェクトなので、まだまだ知らない人も多いはずです。Apache Arrowが早く広まってデファクトスタンダードになると「Rubyでデータ処理」という文脈でもうれしいので、Rubyに関係あろうがなかろうが、Apache Arrowのイベントを開催したい場合はぜひお声がけください。日本でのApache Arrowの第一人者（？少なくともApache Arrowの日本人コミッター第1号）として協力できるはずです。

ちなみに、今度の日曜日（2017年5月28日）に[大阪でApache Arrowオンリーイベント](https://classmethod.connpass.com/event/56478/)があります。関西在住の方はぜひお越しください。現時点で定員オーバーですが、これからキャンセルが増えると思うので参加できるのではないかと思います。
