---
tags: []
title: IBus変換エンジンでXfce4でもシンボルをステータスに表示できるようにするには
---
### はじめに

IBus変換エンジンの実装によっては、GNOME ShellとXfce4というようにデスクトップ環境によって違いがでることがあります。
今回は `IBusProperty` のシンボルを設定していても、Xfce4では表示されない場合の対処方法を紹介します。
<!--more-->


### 問題の詳細

以前、IBusエンジンでのメニューはどのように構成されているのかについて、[IBus変換エンジンでネストしたメニューを正しく表示できるようにするには]({% post_url 2018-06-27-index %})という記事で簡単に解説しました。
`IBusProperty` はメニューの個々の項目を作成するときに使うものです。

参考までに、前回紹介したコードの一部を抜粋します。

```c
IBusProperty *menu = ibus_property_new("InputMode",
                                       PROP_TYPE_MENU,
                                       label,
                                       NULL,
                                       NULL,
                                       TRUE,
                                       TRUE,
                                       PROP_STATE_UNCHECKED,
                                       submenulist);
```


`IBusProperty` は `ibus_property_new` を使って作成します。
では、メニューを選択したときにステータスの表示を変更するにはどうすればよいでしょうか。

その場合には、 `ibus_property_set_symbol` を使ってプロパティのシンボルを設定します。
シンボルは `IBusText` に設定できるものであればよいので、「あ」とか「A」とか好きなものを設定できます。

しかし、実際にGNOME ShellとXfce4それぞれで試すとどうなるでしょうか。

  * GNOME Shellでサンプルプログラムを実行した場合

![GNOME Shellの場合]({{ "/images/blog/20180706_0.png" | relative_url }} "GNOME Shellの場合")

GNOME Shellの場合には、スクリーンショットのようにシンボルを変更するとステータスの表示もかわります。

  * Xfceでサンプルプログラムを実行した場合

![Xfceの場合]({{ "/images/blog/20180706_1.png" | relative_url }} "Xfceの場合")

Xfceの場合、シンボルを変更してもデフォルトの歯車アイコンのままステータスの表示はかわりません。

### シンボルをステータスに表示できるようにするには

解決策は、IBus変換エンジンのコンポーネントの設定ファイルに `<icon_prop_key>InputMode</icon_prop_key>` を追記します。

```
diff --git a/change-inputmode-symbol/changeinputmodesymbol.xml b/change-inputmode-symbol/changeinputmodesymbol.xml                         
index a476008..b083fa1 100644
--- a/change-inputmode-symbol/changeinputmodesymbol.xml
+++ b/change-inputmode-symbol/changeinputmodesymbol.xml
@@ -17,6 +17,7 @@
       <author>Kentaro Hayashi</author>
       <icon></icon>
       <layout>default</layout>
+      <icon_prop_key>InputMode</icon_prop_key>
       <longname>ChangeInputModeSymbol</longname>
       <description>Change Input Mode Symbol (Japanese Input Method)</description>                                                         
       <rank>50</rank>
```


![icon_prop_keyの効果]({{ "/images/blog/20180706_2.png" | relative_url }} "icon_prop_keyの効果")

これでXfce4でも期待通りにステータスにシンボルが表示されるようになります。

### まとめ

今回はデスクトップ環境によらずシンボルをステータスに表示できるようにする方法を紹介しました。
