---
tags:
- mozilla
title: Fx Meta Installerを使った、カスタマイズ済みのFirefoxのインストーラーの作り方
---
クリアコードでは、[Firefoxサポート事業やThunderbirdサポート事業](/services/mozilla/menu.html)の一環として、ユーザー企業さまの社内で使うためにFirefoxやThunderbirdを一括導入するお手伝いを承っております。その際、クリアコードでは「Fx Meta Installer」というソフトウェアを利用しています。[Fx Meta InstallerはGitHubにて公開しています](https://github.com/clear-code/fx-meta-installer/)ので、誰でも自由に利用することができます。
<!--more-->


この記事では、Fx Meta Installerの概要と、簡単な利用手順を解説します。

2014年10月24日：最新のFx Meta Installerの仕様に合わせて、説明を若干更新しました。

  1. はじめに

       1. Fx Meta Installerとは何か？

       1. Fx Meta Installerの実態

       1. 使用・利用の条件について

  1. 必要なもの

       1. Windowsでのビルド環境の構築

       1. Linuxでのビルド環境の構築（Debian、Ubuntu）

  1. ビルドしてみよう

       1. ビルド用設定ファイルの作成

            1. インストールするアプリケーションの設定

            1. Firefoxのバージョンの指定

            1. Firefoxのインストーラーの取得方法に関する設定

            1. サイレントインストールやウィザードの挙動に関する設定

            1. 既定のブラウザの設定

       1. 同梱するファイルの準備

            1. Firefoxのインストーラー

            1. アドオン

            1. 集中管理用の設定ファイル

       1. ビルドする

            1. ビルドスクリプトの実行

            1. メタインストーラーのテスト実行

            1. 配布用メタインストーラーの作成

  1. おわりに


### はじめに

#### Fx Meta Installerとは何か？

Firefoxのインストーラーにはサイレントインストール[^0]機能があります。しかしながら、この機能ではFirefoxのインストール先やショートカットの作成の有無などは制御できるものの、Firefoxの細かい設定までは変更することができません。設定や挙動を変えた状態にするためには、Firefoxをインストールした後で変更を行うか、もしくは、ソースコードを変更した上で独自ビルドを作成するかのどちらかの方法を選ぶ必要があります。

Fx Meta Installerは、前者の方法でのカスタマイズを支援するソフトウェアです。Fx Meta Installerの最終生成物はWindows用の実行ファイル（インストーラー）となり、これを実行すると、Firefoxのインストール、設定の変更（同梱の設定ファイルの設置）、および同梱したアドオンのインストールを自動的に完了できます。Firefoxのインストーラーは同梱することもできますし、ネットワーク経由で自動的に取得してくることもできます。「Firefoxのインストーラーをキックするインストーラー」ということで、「Fx Meta Installer」という名前にしました。

#### Fx Meta Installerの実態

Fx Meta Installerは、2つの実行ファイルを作成します。1つは、実際のインストール処理を行うモジュール「fainstall」。もう1つは、実際に頒布することになる最終生成物の実行ファイル「メタインストーラー」です。メタインストーラーの実態は、fainstallとその他の頒布物を併せて7−Zip形式で圧縮した自己解凍書庫ファイルとなります。

Fx Meta Installerは元々、以下のような意図の元で開発されていました。

  * Firefoxを「Firefox用のアドオン形式で開発されたアプリケーションを実行するための依存ライブラリ」と見なして、アドオンのminVersion/maxVersionの条件を満たすバージョンのFirefoxがまだインストールされていなければ（依存関係が満たされていなければ）Firefoxをインストールする。依存関係が満たされていれば、何もしない。
  * その上で、Firefoxの組み込みのモジュール[^1]としてアドオンをインストールする。

その後、カスタマイズ内容をアドオンの形式で提供することの利点に着目し、FirefoxやThunderbirdの導入案件において利用しやすいよう機能の拡張を行ってきました。その結果、現在ではFirefoxとアドオンの同時インストールだけでなく、集中管理用の設定ファイルの設置や、Firefoxに任意のオプションを指定した状態で起動するためのショートカットの作成なども行えるようになっています。

#### 使用・利用の条件について

fainstallについては、使用および利用にあたっての条件は特にありません。個人的利用、法人での利用、商用利用など、自由に使っていただいて問題ありません。

それに対して、最終的な頒布物となるメタインストーラーの使用および利用の許諾条件は、そのメタインストーラーによってインストールされるソフトウェア自体の使用および利用の許諾条件に依存しますので、くれぐれもご注意下さい。例えば、以下のような制限があり得ます。

  * 同梱したアドオンやプラグインの利用許諾条件として、非営利での利用が必須条件となっている場合、メタインストーラーを有償で販売することはできません。
  * Firefoxを自動的にインストールするように設定した場合、メタインストーラーを不特定多数に向けて無断で一般公開することはできません。一般公開のためにはMozillaの許諾を得る必要があります。詳しくは[法人向けFAQの「製品をカスタマイズして配布しても構わないのですか？」の項](http://www.mozilla.jp/business/faq/#sec-licensing)を参照して下さい[^2]。

### 必要なもの

Fx Meta Installerは、FirefoxのWindows版インストーラーと同じく、[Nullsoft Scriptable Install System](http://nsis.sourceforge.net/Main_Page)（NSIS）によって開発されています。よって、fainstallおよびメタインストーラーをビルドするためには、NSISスクリプトのビルド環境を用意する必要があります。各プラットフォームでのビルド環境構築の手順は以下の通りです。

#### Windowsでのビルド環境の構築

  1. [NSIS](http://nsis.sourceforge.net/Main_Page) v2.46をダウンロードし、インストールします[^3]。

  1. 以下に挙げる、必要なプラグインをインストールします。DLLファイルはC:\Program Files\NSIS\Plugins以下に、ヘッダーファイル[^4]はC:\Program Files\NSIS\Include以下にインストールして下さい[^5]。

       * [FindProcDLL](http://nsis.sourceforge.net/FindProcDLL_plug-in)
       * [InetLoad](http://nsis.sourceforge.net/InetLoad_plug-in)
       * [ZipDLL](http://nsis.sourceforge.net/ZipDLL_plug-in)
       * [CustomLicense](http://nsis.sourceforge.net/CustomLicense_plug-in)
       * [XML](http://nsis.sourceforge.net/XML_plug-in)
       * [MD5](http://nsis.sourceforge.net/MD5_plugin)
       * [UAC](http://nsis.sourceforge.net/UAC_plug-in)
       * [AccessControl](http://nsis.sourceforge.net/AccessControl_plug-in)
       * [UserMgr](http://nsis.sourceforge.net/UserMgr_plug-in)
  1. 「config.bat.sample」をコピーし、「config.bat」という名前にします。必要に応じてNSISのインストール先のパスを修正します。例えば、64bit版のWindows 7でNSISを既定の設定でインストールした場合は、「C:\Program Files (x86)\NSIS」と書き換えます。


#### Linuxでのビルド環境の構築（Debian、Ubuntu）

  1. 必要なパッケージをインストールします。

     {% raw %}
```
$ sudo apt-get install nsis
```
{% endraw %}
  1. 必要なプラグインをインストールします。プラグインの一覧は「Windowsでのビルド環境の構築」と同様です。DLLファイルは/usr/share/nsis/Plugins以下に、ヘッダーファイルは/usr/share/nsis/Include以下にインストールして下さい。


### ビルドしてみよう

それでは、以下のような仕様のメタインストーラーを作成するという想定で、ビルド手順を順番に解説していきましょう。

  * 必ずFirefox 33.0をインストールする。インストーラーの実行ファイルは同梱する。
  * ウィザードを使わず、実行したら所定の設定でインストールが自動的に完了するものとする。
  * インストール後、Firefoxを既定のブラウザとする。
  * インストール中、何が起こっているか分かるように、進行状況のプログレスバーは表示する。
  * [システムモニター](https://addons.mozilla.org/firefox/addon/system-monitor)を同梱する。
  * 初期設定として、Firefoxとアドオンの自動更新を無効化する。また、この設定をロックして、ユーザーが自分で有効化できないようにする。

#### ビルド用設定ファイルの作成

Fx Meta Installerの設定は、ビルド時と、ビルド後の2つのタイミングで行うことができます。基本的にはビルド時の設定だけで問題ありませんが、NSISのビルド環境が整っていない環境でも細かい挙動を調整できるように、一部の設定はビルド後も「fainstall.ini」というファイルを使って変更できるようになっています。また、機能の中にはfainstall.iniに設定を書き加えることでしか利用できないものもあります。

ビルド時の設定は、config.nshで行います。サンプルファイルが「config.nsh.sample」という名前で含まれていますので、最初はこれをコピーして使ってください。

このファイルでは、以下の書式で設定を記述します。

{% raw %}
```
!define 設定名 "設定値"
```
{% endraw %}

サンプルファイルの内容のままビルドした場合、できあがるメタインストーラーは以下のような仕様になります。

  * Firefoxをインストールする。
  * Firefoxのバージョンは10から99までの間を想定する。
  * Firefoxのダウングレードは行わない。
  * Firefoxのインストーラーが同梱されていない場合は、インストールを中断する。
  * ウィザードを表示する[^6]。
  * Firefoxのインストーラーのウィザードは表示しない。
  * Firefoxを既定のブラウザとしない（現在の状態を維持する）。

では、それぞれの設定を必要に応じて変更していくことにします。

##### インストールするアプリケーションの設定

Fx Meta Installerは、FirefoxとThunderbirdのどちらかを選択してインストールできるようになっています。Thunderbird用のメタインストーラーを作成する場合には、以下の箇所を変更します。

{% raw %}
```
!define APP_NAME "Thunderbird" ; "Firefox" から変更する
```
{% endraw %}

今回はFirefox用のメタインストーラーを作成しますので、この部分の設定は変更しません。

##### Firefoxのバージョンの指定

Fx Meta Installerでは、Firefoxのバージョンについて細かい指定ができるようになっています。サンプルの設定は、以下のようになっています。

  * Firefoxが全くインストールされていない場合は、Firefoxを新たにインストールする。
  * 10.0から99.99までの間のいずれかのバージョンのFirefoxがインストールされている場合は、何もしない[^7]。
  * 10.0よりも古いバージョンのFirefoxがインストールされている場合は、上記の範囲のバージョンのFirefoxにアップグレードする。
  * 99.99よりも新しいバージョンのFirefoxがインストールされている場合は、インストール処理全体を中止する。

今回はFirefox 33.0のインストールを行うようにしたいところですが、サンプルのままだと、Firefox 10やFirefox 11のように古いバージョンがインストールされている環境では、Firefoxのインストール処理がスキップされてしまいます[^8]。そこで、以下の通り変更します。

{% raw %}
```
!define APP_MIN_VERSION "33.0" ; "10.0" から変更する
```
{% endraw %}

また、Firefox 34やFirefox 35のように新しいバージョンがインストールされている環境でも、やはりFirefoxのインストール処理はスキップされてしまいます。
企業向けの導入においては、システム管理部門で検証済みのバージョンのみ許可し、検証されていないバージョンは、たとえセキュリティアップデートであっても導入を許可しない、という風にFirefoxのバージョンを厳密に固定する場合があります。
今回は新しいバージョンのFirefoxが導入済みの場合でも必ずFirefox 33.0をインストールするようにしたいので、以下の通り変更します。

{% raw %}
```
!define APP_MAX_VERSION "33.0" ; "99.99" から変更する
!define APP_ALLOW_DOWNGRADE      ; コメントアウトを外す
```
{% endraw %}

##### Firefoxのインストーラーの取得方法に関する設定

Fx Meta Installerは、FirefoxやThunderbirdのインストーラーが同梱されている場合にはそれを使い、同梱されていない場合にはNASに置かれたファイルを利用し、それもなければWebサーバーからインストーラーを自動的にダウンロードしてくる、という風に、Firefoxのインストーラーの取得元を順番にフォールバックしていく機能を含んでいます。これは、以下の箇所のコメントアウトを外して適切な設定を行うことで利用できます。

{% raw %}
```
;!define APP_DOWNLOAD_PATH "\\fileserver\shared\Firefox Setup 15.0.1.exe"
;!define APP_DOWNLOAD_URL  "http://download.mozilla.org/?product=firefox-15.0.1&os=win&lang=ja"
;!define APP_HASH          "8d017402de51a144d0e0fe4d3e2132cb"
```
{% endraw %}

サンプルからわかる通り、取得したインストーラーが確かに想定しているファイルと同一かどうかを確かめられるように、MD5チェックサムを指定することもできます[^9]。

なお、いずれの方法でもインストーラーを取得できなかった場合には、メタインストーラーはインストール処理を中止します。

今回はネットワーク経由での取得は行わないため、上記の箇所は変更しません。

##### サイレントインストールやウィザードの挙動に関する設定

Fx Meta Installerはサンプルの状態では、一般的な「インストールの可否を訊ねる」「EULA（エンドユーザー向けの利用許諾書）への同意を求める」といったウィザードを表示するようになっています。

企業向けの導入においては、ログイン時に特定のファイルを自動的に実行するなどの方法で一括導入を行う場合があり、そのようなケースではウィザードの操作を省略する必要があります。そのようなニーズに対応するため、Fx Meta Installerでは2段階のサイレントインストールが可能となっています。

ウィザードやダイアログの類を一切表示しない完全なサイレントインストールを行うようにしたい場合は、以下のように変更します。

{% raw %}
```
!define PRODUCT_INSTALL_MODE "QUIET" ; "NORMAL" から変更する
```
{% endraw %}

ウィザードの操作はさせたくないけれどもインストールの進行状況だけは表示させたいという場合[^10]は、以下のようにします。

{% raw %}
```
!define PRODUCT_INSTALL_MODE "PASSIVE" ; "NORMAL" から変更する
```
{% endraw %}

今回は、ウィザードの操作は省略しつつインストールの進行状況を表示したいので、「PASSIVE」に設定します。

##### 既定のブラウザの設定

Fx Meta Installerでは、インストールしたFirefoxをシステムの既定のブラウザにするかどうか[^11]も設定できます。今回はFirefoxを既定のブラウザにしたいので、以下の通り変更します。

{% raw %}
```
!define DEFAULT_CLIENT "StartMenuInternet\FIREFOX.EXE" ; コメントアウトを外す。
```
{% endraw %}

なお、Thunderbirdをインストールする場合で、Thunderbirdを既定のメールクライアントにしたい場合は、以下のように設定します。

{% raw %}
```
!define DEFAULT_CLIENT "Mail\Mozilla Thunderbird"
```
{% endraw %}

以上で、メタインストーラの設定は終了です。これ以外にも色々な設定項目がありますが、今回は解説を省略します[^12]。

#### 同梱するファイルの準備

メタインストーラーの挙動を設定できたら、次は、メタインストーラーに含めるファイルを用意します。

メタインストーラーに含めるファイルは、「resources」という名前のフォルダの中に配置します。resourcesフォルダは初期状態では用意されていないので、新しく作成して下さい。

##### Firefoxのインストーラー

メタインストーラーにFirefoxのインストーラーを同梱するには、ダウンロードしたインストーラーの実行ファイル（「Firefox Setup 33.0.exe」など）をresourcesフォルダに置きます。

今回はFirefox 33.0を同梱したいので、まずはMozillaの公式サイトからFirefox 33.0のリリース版のWindows用インストーラーをダウンロードします。

使用するインストーラは、すべてのファイルを含んだ完全版のインストーラである必要があります。
インストール中に追加のファイルをダウンロードする種類のインストーラは、メタインストーラーに含めることができませんのでご注意ください。
完全版のインストーラは、以下のようなURLで入手できます。

  * [http://download.cdn.mozilla.net/pub/mozilla.org/firefox/releases/](http://download.cdn.mozilla.net/pub/mozilla.org/firefox/releases/)33.0/win32/ja/Firefox%20Setup%2033.0.exe
  * [http://download.cdn.mozilla.net/pub/mozilla.org/firefox/releases/](http://download.cdn.mozilla.net/pub/mozilla.org/thunderbird/releases/)31.2.0/win32/ja/Thunderbird%20Setup%2031.2.0.exe

ダウンロードしたファイルは「Firefox Setup 33.0.exe」という名前になっていますので、そのままresourcesフォルダに置いて下さい。
メタインストーラは、「Firefox Setup *.exe」や「Firefox-setup.exe」のような名前のファイルが存在していれば、それをFirefoxのインストーラとして自動的に検出します。

なお、ウィザードを表示するよう設定してメタインストーラーを作成する場合は、ウィザードの途中で[FirefoxのEULA](http://www.mozilla.org/en-US/legal/eula/firefox.html)（[参考訳](http://www.mozilla.org/ja/legal/eula/firefox.html)）に同意する画面が表示されるので、そのためのEULAのテキストファイルもを含めておく必要があります。この場合は、EULAの文章をテキストファイルとして保存し、resourcesフォルダに「Firefox-EULA.txt」という名前で置いて下さい。

Firefoxのインストーラーを同梱しなかった場合、メタインストーラーはビルド時の設定に従ってNASおよびWebサーバーからインストーラーの取得を試みます[^13]。

##### アドオン

メタインストーラーにアドオンを同梱するには、そのアドオンのXPIパッケージをresourcesフォルダに置きます。
Firefoxには[インストール先の distribution/bundles/ 以下に置かれたアドオンをモジュールとして組み込んだ状態で起動する仕組み](http://mike.kaply.com/2012/02/09/integrating-add-ons-into-firefox/)があり、メタインストーラーはこの仕組みを利用して、アドオンをFirefoxにインストールします。

今回は[システムモニター](https://addons.mozilla.org/firefox/addon/system-monitor)を同梱したいので、XPIパッケージ「system_monitor-0.6.3-fx+tb-win32.xpi」をダウンロードし、resourcesフォルダに置いて下さい。

なお、この方法でFirefoxのモジュールとして組み込まれたアドオンは、完全にFirefoxの一部として扱われるため、アドオンマネージャーには表示されず、セーフモードでも無効化されることはありません。ユーザが任意に設定を変更できるようにするためには、アドオンマネージャー以外の場所から設定画面にアクセスできるようにしておくか、アドオンのインストール先を distribution/bundles/ 以下ではなく extensions/ 以下に変更する必要があります。
アドオンのインストール先を変更する方法については、Fx Meta Installerの構成ファイルの中のdoc/usage.txt.jaを参照して下さい。

また、アドオンは何個でもインストールできます。インストールしたいアドオンが複数ある場合は、すべてのアドオンのXPIファイルをresourcesフォルダに置いて下さい。

##### 集中管理用の設定ファイル

メタインストーラーには、[集中管理用の設定ファイル](https://developer.mozilla.org/ja/docs/MCD/Getting_Started)を適切な場所にインストールする機能も含まれています。具体的には、拡張子が「.cfg」であるファイルをresourcesフォルダに置くとFirefoxのインストール先のフォルダに、拡張子が「.js」であるファイルをresourcesフォルダに置くとFirefoxのインストール先の defaults/pref/ 以下にインストールされます。

今回はFirefoxとアドオンの自動更新を無効化し、同時に、その設定をロックしたいので、まずは自動更新を無効化する設定をロックする集中管理用の設定ファイルを作成します。以下の内容のファイルを作成し、resourcesフォルダに「autoconfig.cfg」という名前で保存して下さい。

{% raw %}
```
// 最初の行は無視されるため、必ずコメント行とする。
lockPref("app.update.enabled", false);
lockPref("extensions.update.enabled", false);
```
{% endraw %}

次に、集中管理用の設定ファイルを読み込ませるための設定ファイルを作成します。以下の内容のファイルを作成し、resourcesフォルダに「autoconfig.js」という名前で保存して下さい。

{% raw %}
```
pref("general.config.filename", "autoconfig.cfg");
pref("general.config.vendor", "autoconfig");
pref("general.config.obscure_value", 0);
```
{% endraw %}

#### ビルドする

以上で、すべての必要なファイルが準備できました。いよいよビルドしてみましょう。

##### ビルドスクリプトの実行

設定が完了し、必要なファイルを準備できたら、Windows環境であれば「make.bat」を、Linux環境であれば「make.sh」を実行して下さい。ビルドに成功すると、メッセージの最後に「Success」と出力され、「FxMetaInstaller-source」という名前のフォルダが作成されます。このフォルダには、配布用メタインストーラーの構成ファイル一式と、配布用メタインストーラーの実行ファイルを作成するためのバッチファイルが収められています。

ビルドに失敗した場合、メッセージの最後に「Failed to build fainstall.exe!」と表示されます。ビルド時の設定に間違いがないか、必要なプラグインがすべてインストールされているかなどを再確認して下さい。

##### メタインストーラーのテスト実行

ビルドに成功した場合であっても、resourcesフォルダの内容などにミスがあった場合、期待通りの実行結果を得られません。そのため、この時点でテスト実行しておくことをお薦めします。

テスト用のWindows環境のローカルディスク上に「FxMetaInstaller-source」フォルダを置き、フォルダ内の実行ファイル「fainstall.exe」を実行して下さい。メタインストーラーを実行した場合と同じ処理が行われ、Firefoxのインストールやアドオンのインストールなどが行われます。インストールされたFirefoxの設定が期待通りになっているか、同梱したアドオンが認識されているかなどを確認して下さい。

##### 配布用メタインストーラーの生成

テスト実行で問題がなければ、配布用のメタインストーラーの実行ファイルを作成します。Windows環境のローカルディスク上に「FxMetaInstaller-source」フォルダを置き、その中にあるバッチファイル「FxMetaInstaller.bat」を実行して下さい。この操作により、配布用のメタインストーラーの実行ファイル「FxMetaInstaller.exe」が作成されます。

なお、「FxMetaInstaller-source」フォルダに出力される設定ファイル「fainstall.ini」を編集したり、resourcesフォルダの内容を変更すると、再ビルドが必要ない範囲でメタインストーラの挙動やインストール対象のファイルを変更することができます[^14]。設定の変更後は、バッチファイル「FxMetaInstaller.bat」を実行して配布用のメタインストーラーの実行ファイルを再作成して下さい。

### おわりに

Fx Meta Installerは、上記の例で行った以外にも様々な設定が可能です。詳しい使い方はdocフォルダの中のドキュメントに解説がありますので、参照してみて下さい。

[^0]: ウィザードを使わずに、あらかじめ決められた通りの設定でインストールを行うこと

[^1]: ユーザが自分でインストールするアドオンとは異なり、どのユーザでFirefoxを起動してもそのアドオンが組み込まれた状態で起動することになるアドオン。Nortonツールバーなどがこの形式をとっている

[^2]: なお、公式のFirefoxのバイナリではなく、Firefoxのロゴなどを含まない独自ビルドのバイナリであれば、この限りではありません。

[^3]: デバッグ時には、[デバッグメッセージの出力に対応したバージョン](http://nsis.sourceforge.net/Special_Builds)を使うと、詳細なログを参照することができます。

[^4]: 拡張子が「.nsh」であるファイル

[^5]: 64bit版Windowsでは、NSISはC:\Program Files(x86)\NSIS以下にインストールされます。この場合、プラグインのインストール先もC:\Program Files(x86)\NSIS\PluginsおよびC:\Program Files(x86)\NSIS\Include以下となりますので注意して下さい。

[^6]: サイレントインストールではない。

[^7]: そのまま処理を継続する

[^8]: 依存関係が満たされていると判断するため

[^9]: 現状では、SHA1などでのハッシュ値を計算するためのNSIS用プラグインにはバグがあり利用できないため、MD5を使用しています。

[^10]: ウィザードではないので実行を止めることはできません。

[^11]: Thunderbirdであれば、既定のメールクライアントにするかどうか

[^12]: 詳しくはFx Meta Installerの構成ファイルの中のdoc/usage.txt.jaを参照して下さい。

[^13]: 取得できなければ、エラーとなりインストール処理を中止します。

[^14]: fainstall.iniでどのような設定が可能かや、どのようなファイルをインストール対象にできるかについては、doc/usage.txt.jaを参照して下さい。
