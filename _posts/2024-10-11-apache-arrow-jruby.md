---
tags:
- apache-arrow
- ruby
title: JRubyでもApache Arrowを使いたい？
author: kou
---
[Apache Arrow](https://arrow.apache.org/)の開発に参加している須藤です。現時点で[apache/arrowのコミット数](https://github.com/apache/arrow/graphs/contributors)は1位です。私はRubyでデータ処理できるようになるといいなぁと思ってApache Arrowの開発に参加し始めました。同じような人が増えるといいなぁと思ったりなにか試したりしましたが、あいかわらず、今でも実質1人でApache ArrowのRuby対応をしています。何度目かの「もっと仲間を増やさないと！」という気持ちになったので、最近の活動を紹介して仲間を増やそうと試みます。

そのために選んだ話題がJRubyなのはちょっとアレな気もしますが、最近「がんばったな！」という気持ちになったのがこれだからしょうがない。

<!--more-->

### JRubyでApache Arrow？

JRubyでもApache Arrowを扱いたいというのは[RubyKaigi 2023でADBCの話をした]({% post_url 2023-05-15-rubykaigi-2023 %})あとに[JRuby開発者の@headiusから提案](https://github.com/apache/arrow/issues/35589)されました。そのときはちょっと頑張ったんですが、依存ライブラリーの管理をどうしようかなぁというところで止まっていました。

[RubyKaigi 2024で@headiusと相談]({% post_url 2024-05-22-rubykaigi-2024-code-party-report %})して、いけそうな気持ちにはなったんですが、やる気がしなくて手つかずのままでした。

でも、この間、[ちょっといろいろあってFiddleのJRuby実装をruby/fiddleに取り込む](https://github.com/ruby/fiddle/pull/147)というのをやってちょっとやる気がでました。私は普段JRubyは触っていなくて、昔、[ActiveLdapのJNDIアダプター](https://github.com/activeldap/activeldap/blob/master/lib/active_ldap/adapter/jndi_connection.rb)を書いたときに触ったとかそのくらいの経験だったのですが、久しぶりに触ったのでちょっとやる気がでたのです。

やる気がでたので[とりあえず動く](https://github.com/apache/arrow/pull/44346)くらいのところまでやりました。とりあえず動くくらいなので、まだまだ実用的ではないのですが、そのくらいの方が参加しやすい（仲間が増えやすい）んじゃないかとも思うので紹介します。

### Apache Arrow実装

Apache Arrowの実装は大きく、ネイティブ実装とバインディングに分かれます。ネイティブ実装はその言語で一から実装しているやつで、バインディングは他の言語でのネイティブ実装を使えるようにしたものです。

RubyのApache Arrow実装はC++ネイティブ実装のバインディングです。

JRubyのApache Arrow実装もバインディングですが、Javaネイティブ実装のバインディングです。JNIとかFFIとかいうやつを使ったりすればJRubyからもC++ネイティブ実装を使えるのですが、JRubyならJavaネイティブ実装を使ったほうがカッコいいのでJavaネイティブ実装を使っています。

### RubyのApache Arrow実装のAPI

実は、C++ネイティブ実装とJavaネイティブ実装はAPIが結構違います。なので、それぞれのネイティブ実装のAPIをそのままRubyユーザーに提供すると、CRubyのときとJRubyのときで書き方が全然変わってしまいます。

それだと使い勝手がビミョーなので、JRubyのときでも既存のRuby実装のAPIで使えるようにしています。

たとえば、C++ネイティブ実装では配列のことを`Array`と呼んでいますが、Javaネイティブ実装では`Vector`と呼んでいます。Ruby実装のAPIではC++ネイティブ実装を使っているときでもJavaネイティブ実装を使っているときでも`Array`と呼んでいます。これは、既存のRuby実装のAPIがC++ネイティブ実装ベースのため、C++ネイティブ実装の名前を使っていたからです。

ということで、JRubyのApache Arrow実装はJavaネイティブ実装のAPIをラップして既存のRuby実装のAPI（これはC++ネイティブ実装ベースのAPI）で使えるようにしたものということになります。

### JRubyのApache Arrow実装の現状

とりあえず動くものがどのくらいのものかというと、8ビット符号付き整数の配列と32ビット符号付き整数の配列を作れるだけです。Apache Arrowファイルを読んだり書いたりすることはできません。

次は、よくある型（他の整数型や浮動小数点型や文字列型など）の配列に対応し、それからApache Arrowのファイルを読み書きできるようにするところかなぁという感じです。

JRubyを動かせる環境を準備できればそんなに難しくないので、これを機にApache Arrowのことを知りたいなぁという人には向いている出だしじゃないかな。ちなみに、私は`docker run -it --rm -v $PWD:/host jruby bash`で環境を用意しています。

ちょっとやってみようかなという人は[Red Data Toolsのチャット](https://app.element.io/#/room/#red-data-tools_ja:gitter.im)に来てください！

### まとめ

Apache ArrowのRuby対応を一緒にやってくれる仲間が増えて欲しかったので、最近がんばったなと思ったJRuby対応を紹介しつつ仲間を募集してみました。仲間が増えるといいな。

それはそうとして、apache/arrowコミット数1位の私にApache Arrow関連のサポートを頼みたいという場合は[クリアコードのApache Arrowサービス]({% link services/apache-arrow.md %})をどうぞ。
