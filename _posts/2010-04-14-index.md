---
tags:
- cutter
title: Cutter 1.1.3リリース
---
C・C++言語用の単体テストフレームワーク[Cutter](/software/cutter.html)のバージョン1.1.3をリリースしました。
<!--more-->


  * [[ANN] Cutter 1.1.3](http://sourceforge.net/mailarchive/forum.php?thread_name=20100414.152659.1019941042896176977.kou%40clear-code.com&forum_name=cutter-users-ja)

### ハイライト

今回のリリースでは[データ駆動テスト](/search/%E3%83%87%E3%83%BC%E3%82%BF%E9%A7%86%E5%8B%95%E3%83%86%E3%82%B9%E3%83%88/)のサポートを強化しています。サポートしている型が増えたので、これまで以上にテストデータを作りやすくなっています。

例えば、このようにテストデータを作ることができます。

{% raw %}
```c
gcut_add_datum("normal case",
               "expected", G_TYPE_CHAR, 'e',
               "input", G_TYPE_STRING, "Cutter",
               "n", G_TYPE_UINT, 4,
               NULL);
```
{% endraw %}

詳しくはリファレンスマニュアルの[便利なテストデータ用API - gcut_add_datum()](http://cutter.sourceforge.net/reference/ja/cutter-Convenience-test-data-API.html#gcut-add-datum)あたりを見てください。

### Cutterユーザ（徐々に）増加中

Cutterが今のようなGLibベースの作りになってから2年半くらい経ちますが、徐々にユーザが増えてきました。最近、[nfc-tools](http://code.google.com/p/nfc-tools/)という[近距離無線通信](https://ja.wikipedia.org/wiki/%E8%BF%91%E8%B7%9D%E9%9B%A2%E7%84%A1%E7%B7%9A%E9%80%9A%E4%BF%A1)用のツールを開発しているオープンソースプロジェクトでも使われているのを見つけました。

[groonga](http://groonga.org/)もCutterを採用しているプロジェクトで、Cutterのカバレッジ支援機能なども使っています。groongaプロジェクトではCutterが提供している支援機能だけではなく、[コミットした人毎のカバレッジ率](http://groonga.org/stat/)などもグラフ化して公開しています。今後のリリースで、Cutter本体がこのような見せ方を支援する機能を提供したいですね。

### まとめ

Cutter 1.1.3の目玉機能とCutter採用プロジェクトについて紹介しました。

C・C++のプロジェクトで使うテスティングフレームワークを探しているならCutterも検討してみてはいかがでしょうか。
