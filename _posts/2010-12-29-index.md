---
tags: []
title: 2010年まとめ
---
早いもので今年も最後の肉の日になりました。クリアコードのメンバーが2人増え、[第三者割当増資実施](/press-releases/20101029-allocation-of-new-shares-to-third-parties.html)も行い、少しずつですが会社が成長してきた一年でした。
<!--more-->


今回はククログ上で公開した今年の活動をまとめてみます。

### 1月

[あしたのオープンソース研究所](http://oss.infoscience.co.jp/)でオープンソースのマルチメディアフレームワークである[GStreamerを紹介]({% post_url 2010-01-11-index %})しました。今のところ、GStreamerに関する日本語の情報があまりないので、だれかの役にたてているのではないでしょうか。

また、[Debianパッケージの作り方と公開方法]({% post_url 2010-01-18-index %})も紹介しました。これは[Cutter](/software/cutter.html)や[milter manager](/software/milter-manager.html)、[groonga](http://groonga.org/)などで使っている仕組みをまとめたものです。

1月の肉の日は[UxU 0.7.6をリリース]({% post_url 2010-01-29-index %})しました。非同期な処理（イベントのコールバックなど）を簡単にテストするための機能が追加され、より使いやすくなりました。

### 2月

2月はmilterについて2回話してきました。1回目は通信事業者さんの迷惑メール対策に関する勉強会で[milterとmilterで問題になることについて]({% post_url 2010-02-05-index %})話しました。2回目は札幌で行われた[LOCAL DEVELOPER DAY '10 Winter](http://www.local.or.jp/?LDD/LDD10Winter)で、[Rubyでmilterを作る方法について]({% post_url 2010-02-15-index %})話しました。（途中から全文検索の話が入ってきます。）

2月はメールの月でしたね。

### 3月

3月はどこかで話すということはありませんでした。[Debian GNU/Linux上でRPMパッケージを作る方法とYumリポジトリの公開方法]({% post_url 2010-03-03-index %})もCutter、milter manager、groongaなどで使っている方法をまとめたものです。

### 4月

4月もふだん開発している内容をまとめていました。[Muninプラグインの作り方]({% post_url 2010-04-08-index %})や[fat gemの作り方]({% post_url 2010-04-21-index %})をまとめました。fat gemとは複数のRuby用のバイナリが入ったgemのことです。例えば、Ruby 1.8でも1.9でも同じgemを使えるようにしたい場合はfat gemにします。

また、[Rubyのリファレンスマニュアル](http://redmine.ruby-lang.org/wiki/rurema)を全文検索する[るりまサーチ]({% post_url 2010-04-27-index %})を公開したのも4月でした。

### 5月

5月はgit用コミットメール生成スクリプト[git-utils 0.0.1をリリース]({% post_url 2010-05-18-index %})しました。（その後、リポジトリを[GitHubに移行]({% post_url 2010-09-27-index %})しました。9月の話。）

### 6月

6月は[クリアコードのgitリポジトリを公開]({% post_url 2010-06-08-index %})しました。

また[無事に第4期が終わりました]({% post_url 2010-06-30-index %})。ありがとうございます。

### 7月

7月はRails関連のことをいくつか書いています。[Rails 2.3.8に対応したActiveLdap 1.2.2について]({% post_url 2010-07-05-index %})と[Rails 3.0でDeviseでOpenIDを使う方法について]({% post_url 2010-07-13-index %})です。

### 8月

8月はRuby会議の必需品である[名前が大きく書かれた名札を生成するWebアプリケーションを改良]({% post_url 2010-08-18-index %})しました。

### 9月

9月は[日本Ruby会議2010](http://rubykaigi.org/2010/ja/)で話した[るりまサーチについての資料を公開]({% post_url 2010-09-01-index %})[^0]したり、[hbstudy#15](http://heartbeats.jp/hbstudy/2010/08/hbstudy15.html)で[milter managerについて話し]({% post_url 2010-09-20-index %})ました。hbstudy#15の内容は具体的な例を出しながら説明するスタイルなので、milterを知らない人でもわかりやすい内容になっています。

3月から外で話していなかったのですが、8月からまた外で話はじめました。

### 10月

10月は[CPUの使用率とメモリの使用量を表示するFirefoxアドオン「システムモニター」を更新]({% post_url 2010-10-19-index %})していました。

[第三者割当増資実施のお知らせ]({% post_url 2010-10-29-index %})をしたのは10月の肉の日です[^1]。

### 11月

11月は[PDFやオフィス文書からテキストを抜き出すツール「ChupaText」を公開]({% post_url 2010-11-08-index %})しました。

また、[NICT情報通信ベンチャ支援センターのインタビューも公開]({% post_url 2010-11-15-index %})されました。会社としてインタビューされるのは、はじめてのことでした。クリアコードも成長しましたね。

### 12月

12月は[「全文検索エンジンgroongaを囲む夕べ #1」のRuby枠の資料を公開]({% post_url 2010-12-01-index %})しました[^2]。

また、[札幌Ruby会議03](http://regional.rubykaigi.org/sapporo03)で[デバッグ力について]({% post_url 2010-12-08-index %})話しました。

[Ruby Advent Calendar jp: 2010](http://atnd.org/events/10430)にも参加しました。Ruby Advent Calendar jpは今年が初参加です。Rubyで縦書き画像を生成する方法を[地獄のジェネレータ]({% post_url 2010-12-24-index %})を例にして説明しました。

### まとめ

月ごとに今年のクリアコードの活動をまとめました。

[去年]({% post_url 2009-12-21-index %})より発表する機会は減りましたが、毎月いろいろやっていたようです。今年のクリアコードはいかがだったでしょうか。

来年もクリアコードをよろしくお願いします。

[^0]: 日本Ruby会議2010は8月に開催。

[^1]: 実際は2010年8月31日付けで実施していたが、プレスリリースは肉の日まで待った。

[^2]: 「全文検索エンジンgroongaを囲む夕べ #1」は11月の肉の日に開催。
