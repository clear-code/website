---
tags: []
title: Pbuilder と Cowbuilder の性能比較
---
[Ubuntuでdebパッケージのお手軽クリーンルーム（chroot）ビルド環境を構築するには]({% post_url 2014-11-21-index %})で Pbuilder よりも Cowbuilder の方が速いと書いてありますが、本当に Cowbuilder の方が速いのかどうか検証しました。
<!--more-->


検証環境は以下の通りです。

<dl>






<dt>






OS:






</dt>






<dd>


Debian sid amd64


</dd>








<dt>






CPU:






</dt>






<dd>


Intel(R) Core(TM) i7-2620M CPU @ 2.70GHz (8 core)


</dd>








<dt>






Memory:






</dt>






<dd>


8GB


</dd>


</dl>

検証に使うシナリオは以下の通りです。

  1. Debian Wheezy amd64 向けの milter-manager-2.0.5 をビルドします

  1. ネットワークの影響を排除するために一度ビルドして aptcache を用意します

  1. `pbuilder --update`か`cowbuilder --update`でOSを更新する

  1. `pdebuild`でパッケージをビルドする


検証に使ったコードは[milter managerのリポジトリ](https://github.com/milter-manager/milter-manager/tree/master/package/apt)にあります。
ここでは(3)+(4)をスクリプト化して[^0]実行し、時間を計測しました。

いくつかのパターンで計測した結果は次の通りです。

<table>
  <tr><th>ビルド方法</th><th>時間(秒)</th></tr>
  <tr><td>Pbuilder</td><td>241.40</td></tr>
  <tr><td>Cowbuilder</td><td>215.12</td></tr>
  <tr><td>Pbuilder + tmpfs</td><td>99.98</td></tr>
  <tr><td>Cowbuilder + tmpfs</td><td>N/A</td></tr>
</table>


Cowbuilder はハードリンクを使うので原理的に tmpfs を使うことができません。[^1]

### 追加の設定

追加の設定が必要な環境について説明します。

#### Pbuilder + tmpfs

Pbuilder と tmpfs を一緒に使うためには追加の設定が必要です。

tmpfs を用意します。

{% raw %}
```
$ sudo mount -t tmpfs -o size=2g tmpfs /var/cache/pbuilder/build
```
{% endraw %}

pbuilder のデフォルトのビルドディレクトリを tmpfs にします。

pbuilderrc に以下の内容を追加します。

{% raw %}
```
APTCACHEHARDLINK=no
```
{% endraw %}

これで aptcache のコピーにハードリンクを使わなくなります。
`APTCACHEHARDLINK`以外の設定はほぼ全てコマンドラインオプションで指定することができます。それ以外のオプションをどのように使用しているかは[ビルドスクリプト](https://github.com/milter-manager/milter-manager/blob/master/package/apt/build-in-clean-room.sh)を参照してください。

### まとめ

Cowbuilder は Pbuilder よりも速いことがわかりましたが、それよりも Pbuilder + tmpfs の方が速いということがわかりました。
Cowbuilder は Pbuilder でボトルネックとなっていた Disk I/O の量を減らす方法として COW を使っています。
一方、Pbuilder + tmpfs では tmpfs を使うことによって Disk I/O そのものをなくして高速化しています。[^2]

pdebuildを使ったdebパッケージのビルドでは、最小環境にビルドに必要なパッケージを全てインストールしてからパッケージのビルドを開始するので、大量のパッケージをインストールします。
そのため COW を使って変更のあった部分のみディスクに書き込んだとしてもベースイメージからの変更量が多くなるため大量の Disk I/O が発生し、Cowbuilder でも思った程速くなりませんでした。
Pbuilder + tmpfs では大量のパッケージインストールを tmpfs 上で行うため Disk I/O が発生しないので、高速化できました。

[^0]: milter managerのパッケージビルドスクリプトを改造

[^1]: ハードリンクはデバイスをまたいで使うことができないという制限があります。

[^2]: `pbuilder --update`によるtarの展開と再圧縮もtmpfs上で作業するとで速くなっています。
