---
tags: []
title: Ruby-GNOME2 0.17.0リリース
---
昨日、[GTK+](https://ja.wikipedia.org/wiki/GTK%2B)を含む[GNOME](https://ja.wikipedia.org/wiki/GNOME)関連ライブラリの[Ruby](https://ja.wikipedia.org/wiki/Ruby)バインディング集[Ruby-GNOME2](http://ruby-gnome2.sourceforge.jp/ja/)のバージョン0.17.0が[公開](http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-list/45475)されました。（[もう少し細かい変更点が書かれたアナウンス](http://ruby-gnome2.sourceforge.jp/ja/sfmltoj.cgi?key=/mailarchive/message.php%3Fmsg_name%3D20080907.174835.1064051296153662078.kou%2540cozmixng.org)）
<!--more-->


### 目玉

このリリースの目玉はRuby 1.8.7対応です。

以前のバージョンである0.16.0をRuby 1.8.7で動かすと、以下のようなメッセージとともに落ちてしまいます。[^0]

{% raw %}
```
[BUG] object allocation during garbage collection phase
```
{% endraw %}

0.17.0では上記の問題を解決し、Ruby 1.8.7でも動作するようになっています。

他にも、フリーなマルチメディアフレームワークである[GStreamer](https://ja.wikipedia.org/wiki/GStreamer) 0.10.xへの対応など新機能が追加されています。

RubyでGUIを作成したいときは、Ruby-GNOME2を試してみてください。

### 協力のお願い

アナウンスメールにもありますが、Ruby-GNOME2プロジェクトでは協力してくれる方を募集しています。例えば、バインディングを開発してくれる方、ドキュメントを書いてくれる方、英語のドキュメントを日本語化してくれる方、リリース作業をしてくれる方などを募集しています。

興味のある方は[ruby-gnome2-devel-ja ML](https://lists.sourceforge.net/lists/listinfo/ruby-gnome2-devel-ja)までお願いします。

[^0]: 暫定的な対処法はGC.disableでした。
