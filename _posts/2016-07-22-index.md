---
tags:
- groonga
- presentation
title: MariaDBコミュニティイベント in Tokyo：Mroonga 2016 - 高速日本語全文検索 for MariaDB
---
2016年7月21日に「[MariaDBコミュニティイベント in Tokyo]({% post_url 2016-07-12-index %})」というイベントが開催されました。発表中でも随時参加者から質問があがり質疑応答が始まるという、日本のイベントではあまり見られない活発なイベントでした。
<!--more-->


このイベントでMariaDBユーザー向けの高速全文検索ソリューションとして[Mroonga](http://mroonga.org/)を紹介しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/mariadb-community-event-2016-07-21/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/mariadb-community-event-2016-07-21/" title="Mroonga最新情報2016">Mroonga最新情報2016</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/mariadb-community-event-2016-07-21/)

  * [スライド（SlideShare）](https://slideshare.net/kou/mariadb-community-event-2016-07-21)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-mariadb-community-event-2016-07-21)

Mroongaを知らない人向けの資料となっています。MariaDBで日本語全文検索を実現する方法を検討している方はご活用ください。
