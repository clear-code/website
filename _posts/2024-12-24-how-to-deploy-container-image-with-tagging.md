---
title: Fluentdのコンテナイメージをタグを打つだけでデプロイできるようにした方法
author: kenhys
tags:
  - fluentd
---

先日、Fluentdのv1.18.0をリリースしました。

v1.18.0の変更点については[Fluentd v1.18.0をリリース]({% post_url 2024-12-05-fluentd-v1.18.0 %})に関する記事を参照していただくとして、
このリリースから、[Docker Hubで提供しているコンテナイメージ](https://hub.docker.com/r/fluent/fluentd/)の提供方法を改善し、
GitHubでリリース用のタグを打つことで、コンテナイメージをDocker Hubへとデプロイできるようにしました。

本記事ではどのようにしてそのしくみを実現したのかについて概要を説明します。

<!--more-->

### 従来のコンテナイメージのデプロイ方法

これまでは、FluentdはSponsored OSSでありDocker Free Teamとして扱われていました。
この状態だと、Automated BuildsとよばれるコンテナイメージをWebインターフェースから簡単にビルドできる仕組みが利用可能でした。

しかし、Sponsored OSSであり、Docker Free Teamに所属していても、いつからかAutomated BuildsのWebインタフェースが利用できなくなりました。

したがって、これまでのようにWebインタフェースから簡単にコンテナイメージをビルド・デプロイすることができなくなりました。

`Dockerfile`とイメージをデプロイするためのフックの設定（hub.docker.com側でWebインタフェースと連動して実行される）をメンテナンスしていましたが、その仕組みは使えなくなったのです。
Automated BuildsのWebインタフェースが利用できることを前提としたフックとなっていたので、これらを置き換える必要がでてきました。
そこで、Automated Buildsに依存しない新たなやり方を模索する必要がありました。

### GitHub Actionsへの移行のポイント

Fluentdのコンテナイメージとしては次の5つのタイプのイメージを提供していました。
(Windowsのコンテナイメージだけは別途手動でメンテナンスしています。)

* alpine (非推奨。歴史的経緯でまだ提供している)
* amd64
* arm64
* armhf
* Windows

このうち、Windowsのコンテナイメージを除いてGitHub Actionsでコンテナイメージをビルド、アップロードするようにしました。
具体的な例は[docker-build.yml](https://github.com/fluent/fluentd-docker-image/blob/master/.github/workflows/docker-build.yml)です。

基本的には次のようなことを実施しています。

* 特定のGitタグがpushされたらGitHub Actionsのワークフローを実行する
  * 例: v1.18.0など
* 各イメージに応じたDockerタグの組み合わせのパターンを生成する
  * 例: v1.18.0-debian-amd64-1.0, v1.18-debian-amd64-1, edge-debian-amd64など。v1.18.0-debian-amd64-1.0は特定のイメージに限定して利用するためのDockerタグで、v1.18-debian-amd64-1や edge-debian-amd64はより新しいイメージがリリースされたらそちらを参照できるようにするためのDockerタグ。
* [docker/build-push-action@v6](https://github.com/docker/build-push-action) といった既存のActionを利用して、各プラットフォーム(alpine,amd64,arm64,armhf)ごとのイメージをビルドする

{% raw %}
```
uses: docker/build-push-action@v6
with:
  context: ${{ env.CONTEXT }}
  provenance: false
  push: true
  platforms: linux/amd64
  tags: ${{ env.ALPINETAGS }}
  # dare to use old mediatype (application/vnd.docker.distribution.manifest.v2+json)
  outputs: oci-mediatypes=false
```
{% endraw %}

* `docker buildx imagetools`を利用して、すでにビルドしたイメージに対してマルチアーキテクチャに対応したDockerタグを付与する

{% raw %}
```
   docker buildx imagetools create -t ${{ env.REPOSITORY }}:${MULTIARCH_AMD64_TAG} \
              ${{ env.REPOSITORY }}:${AMD64TAG} \
              ${{ env.REPOSITORY }}:${ARM64TAG} \
              ${{ env.REPOSITORY }}:${ARMHFTAG}
```
{% endraw %}

なお、ベースイメージがすでにマルチアーキテクチャ対応のイメージであり、`Dockerfile`を異なるアーキテクチャ間であっても共通化できている場合には、
次のようなもっとシンプルなルールでマルチアーキテクチャに対応したイメージをビルドできるでしょう。また、個別に`docker buildx imagetools`などを使う必要もないはずです。

{% raw %}
```
uses: docker/build-push-action@v6
with:
  context: .
  file: ${{ env.DOCKERFILE }}
  provenance: false
  push: true
  platforms: linux/amd64,linux/arm64,linux/arm/v7
  tags: ${{ env.TAGS }}
  # dare to use old mediatype (application/vnd.docker.distribution.manifest.v2+json)
  outputs: oci-mediatypes=false
```
{% endraw %}

Fluentdの`Dockerfile`はまだそこまでできていないので、個別のアーキテクチャごとにビルドしてから、最後にDockerタグを付与しなおすということをしています。

ここまで説明した仕組みに移行することで、次のようなメリットがありました。

* GitHubでタグを打つだけでデプロイが完了するので、以前のようにAutomated Buildsの管理画面で明示的にビルドしなくてもよくなった
* そもそもGitタグを付与して管理されてない状態だったので、どの時点の内容でイメージがビルドされたかをトレースできるように改善された
* 従来、amd64とarm64のみマルチアーキテクチャ対応のDockerタグを付与していたが、今回デプロイの仕組みを整備したことで、amd64,arm64だけでなくarmhfにも対応できた

### さいごに

今回は、Fluentdのコンテナイメージをタグを打つだけでデプロイできるようにした方法について概要を説明しました。
今回紹介した方法は、[Fluentdを含めた多種多様なdaemonsetのイメージ](https://github.com/fluent/fluentd-kubernetes-daemonset)を提供する方法としても活用しています。

クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
