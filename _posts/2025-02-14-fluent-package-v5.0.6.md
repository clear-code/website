---
title: LTS版 Fluent Package v5.0.6をリリース
author: kenhys
tags:
  - fluentd
---

2025年2月14日にFluent PackageのLTS(長期サポート)版の最新バージョンであるv5.0.6をリリースしました。

本リリースでは、Windows向けにいくつかの改善を行っています。

本記事ではFluent Packageの変更内容について紹介します。

<!--more-->

### Fluent Package v5.0.6

2025年2月14日にFluent PackageのLTS版の最新バージョンであるv5.0.6をリリースしました。
同梱のRubyを3.2.6から3.2.7に更新し、Fluentdのバージョンはv1.16.6からv1.16.7に更新しています。

本記事ではFluent Packageの変更内容について紹介します。
興味のある方は、以下の公式情報もご覧ください。

公式リリースアナウンス

* Fluent Package v5.0.6: https://www.fluentd.org/blog/fluent-package-v5.0.6-has-been-released

CHANGELOG

* Fluent Package v5.0.6: https://github.com/fluent/fluent-package-builder/releases/tag/v5.0.6

Fluentd 1.16.7の修正内容については、公式アナウンスである[Fluentd v1.16.7 has been released](https://www.fluentd.org/blog/fluentd-v1.16.7-has-been-released)を参考にしてください。

### 改善

Fluent Package v5.0.6では、以下の改善をしています。

* WindowsでFluentdサービスの設定を引き継げるように改善

また、Fluent PackageにFluentd v1.16.7を同梱したことにより、次の問題も修正されています。

* Windowsで`--daemon`オプションを指定して起動しようとすると`NoMethodError`で失敗する
* Windowsでサービスの開始と停止を短期間に行うとプロセスが残留したり、停止できなくなる

#### WindowsでFluentdサービスの設定を引き継げるように改善

従来のバージョンでは、Fluent PackageのWindows版でアップグレード時にサービスの設定が引き継げないという問題がありました。

そのため、Fluent Packageをインストール後に、サービス起動時のオプションをカスタマイズしていた場合には、
パッケージをアップグレードする際に、それまでの設定を再度反映しなおす必要がありました。

今回のリリースにより、Fluent Packageのサービス関連の次のレジストリの値を引き継げるように改善しました。

`HKLM\System\CurrentControlSet\Services\fluentdwinsvc`:

* `Start` (サービスの自動起動の可否)
* `DelayedAutostart` (サービスの遅延起動の可否)
* `fluentdopt` (ログ出力先の設定などをカスタマイズするときに変更する)

### まとめ

今回は、Fluentdの最新パッケージFluent Package v5.0.6 (LTS版)の情報をお届けしました。

パッケージでいくつかの改善を行いました。

長期の安定運用がしやすいLTS版をぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。(FluentdのサポートサービスではEOLとなっているtd-agentからのアップグレードの支援もしています)

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluent-package-builder/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
