---
tags:
- vim
title: Vimでgettext（Sphinx）の翻訳対象ファイルとPOファイルを紐付けて簡単に開けるようにする方法
---
横山です。この記事では、[gettext](https://ja.wikipedia.org/wiki/Gettext)（GNU gettext）を使って翻訳対象ファイルと翻訳ファイル（POファイル）をVimで編集するときの便利設定を紹介します。
<!--more-->


gettextを使ったことがない方は、ククログの過去記事を読んでみると雰囲気がわかるかもしれません。

  * [gettextとバージョン管理システムの相性の悪さを解消する案 - ククログ(2013-11-14)]({% post_url 2013-11-14-index %})

  * [Sphinx 1.3で使えるgettextとバージョン管理の相性の悪さを改善する仕組み - ククログ(2014-12-16)]({% post_url 2014-12-16-index %})

  * [xml2po: XMLをgettextを使って国際化 - ククログ(2011-08-24)]({% post_url 2011-08-24-index %})

gettextによる翻訳は、まず翻訳対象のファイルからメッセージを抽出し、それぞれのメッセージに対応する翻訳をPOファイルに記述するという流れで行われます。翻訳対象のファイルとPOファイルの間を行き来することになるので、その手間を減らすのが今回の目標です。

バッファで開いておく、[NERDTree](https://github.com/scrooloose/nerdtree)などでブックマークしておくといった方法もあるのですが、POファイルの数が多いと探す時間がばかにならないので、翻訳対象ファイルに対応するPOファイルを自動で検出できると効率が上がります。[^0]

ここでは、vim-altrというプラグインを使ってみます。

  * [Vimで「あの」ファイルを即座に開く - TIM Labs](http://labs.timedia.co.jp/2011/07/vim-altr.html)

  * [kana/vim-altr: Vim plugin: Switch to the missing file without interaction](https://github.com/kana/vim-altr)

vim-altrは、設定されたルールに従って、編集中のファイルに関連する別のファイルを開いてくれるプラグインです。
以下のように任意のキーバインドを設定して使います。

```vim
nmap <Leader>a  <Plug>(altr-forward)
```


デフォルトでもルールが用意されているので、各言語の一般的なファイル構成であればこれだけでいい感じに動作してくれるようです。
が、gettextの対象ファイルの配置はプロジェクトによって異なるため、これだけだと動作しないので、vimrcに設定を追加していきます。

ここでは[Groonga](https://github.com/groonga/groonga)プロジェクトを例に説明します。Groongaでは[Sphinx](http://www.sphinx-doc.org/ja/master/)と一緒にgettextを使用していて、ドキュメント構成は以下のようになっています。

  * 翻訳対象ファイル（抜粋）

    * doc/source/news.rst（毎月更新）

    * doc/source/news/5.x.rst（ほとんど更新しない）

    * doc/source/news/6.x.rst（ほとんど更新しない）

    * doc/source/install.rst（ほとんど更新しない）

    * doc/source/install/debian.rst（たまに更新）

    * doc/source/install/centos.rst（たまに更新）

  * POファイル（抜粋）

    * doc/locale/ja/LC_MESSAGES/news.rst

      * news.rstとnews/5.x.rstとnews/6.x.rstが対象

    * doc/locale/ja/LC_MESSAGES/install.rst

      * install.rstとinstall/debian.rstとinstall/centos.rstが対象

上記の例の中では、更新頻度的に一番頻繁に行き来するのはdoc/source/news.rstとdoc/locale/ja/LC_MESSAGES/news.rstの間です。よって、まずはこの2ファイル用の設定を追加してみます。

### 1対1の関係のファイル間の移動

.rst（reStructuredText形式）決め打ちでよいのであれば、以下の1行をvimrcなどに追加するだけです。

```vim
call altr#define('doc/source/%.rst', 'doc/locale/ja/LC_MESSAGES/%.po')
```


これで、doc/source/news.rstとdoc/locale/ja/LC_MESSAGES/news.rstの間を設定したキーバインド（`<Leader>a`など）で行き来することができるようになります。

.rst以外にも対応させておきたい場合、以下のように設定します。

```vim
call altr#define('doc/source/%.*', 'doc/locale/ja/LC_MESSAGES/%.po')
```


（2018/5/10追記: 初稿では2行目として `call altr#define('doc/locale/ja/LC_MESSAGES/%.po', 'doc/source/%.*')` を記載していたのですが、これがなくてもaltr-forwardだけで行き来できました。）

<del>
`<Plug>(altr-back)`に対するキーバインドを設定しておけば上の1行だけで行き来できる（altr-forwordでsourceから.po、altr-backで.poからsource）のですが、2行目を追加しておけば`<Plug>(altr-forward)`だけで行き来できます。好みに応じて設定してください。
</del>


### 多対1の関係のファイル間の移動

頻度は多くありませんが、doc/source/install/debian.rstとdoc/locale/ja/LC_MESSAGES/install.rstの間や、doc/source/install/centos.rstとdoc/locale/ja/LC_MESSAGES/install.rstの間もたまに行き来します。これらは、複数の翻訳対象ファイルに1つのPOファイルが対応しています。このような場合、POファイルから翻訳対象ファイルを検出することは難しいので、翻訳対象ファイルからPOファイルへの一方通行の設定を追加しておくのがおすすめです。具体的には以下のような設定を追加します。

```vim
call altr#define('doc/source/%/%.*', 'doc/locale/ja/LC_MESSAGES/%.po')
```


この設定を追加することで、doc/source/install/debian.rstやdoc/source/install/centos.rstからdoc/locale/ja/LC_MESSAGES/install.rstを簡単に開くことができるようになります。
この場合、翻訳対象ファイルに戻るときは、vim-altrではなく`C-^`（直前に開いていたバッファを開く）などを使うことになります。

細かいところですが、`doc/source/%/%.*`を`doc/source/%/*.*`などとしてしまうと、doc/source/install/debian.rstとdoc/source/install/centos.rstがお互いに行き来する対象になってしまうので注意してください。

後から定義したルールが優先されるようなので、1対1と多対1の設定を両方追加する場合、以下のようにしておくと1対1のルールが優先されます（doc/locale/ja/LC_MESSAGES/news.poからaltr-forwardするときに、doc/source/news/ではなくdoc/source/news.rstが開かれるようになる）。

```vim
call altr#define('doc/source/%/%.*', 'doc/locale/ja/LC_MESSAGES/%.po')
call altr#define('doc/source/%.*', 'doc/locale/ja/LC_MESSAGES/%.po')
```


1対1の関係のファイル間の移動の設定が不要であれば、以下のように.poからaltr-forwardするときは明示的にディレクトリを開くようにしておくのも一案だと思います。

```vim
call altr#define('doc/source/%/%.*', 'doc/locale/ja/LC_MESSAGES/%.po', 'doc/source/%/')
```


または

```vim
call altr#define('doc/source/%/%.*', 'doc/locale/ja/LC_MESSAGES/%.po', 'doc/source/')
```


### まとめ

Vimでgettextの翻訳対象ファイルとPOファイルを紐付けて簡単に開けるようにする方法を紹介しました。
gettext以外にも応用できる方法だと思うので、ぜひ活用してみてください。

[^0]: POファイル自体はVimではなく<a href="https://poedit.net/">Poedit</a>などで編集することが多いと思いますが、Poeditで編集するときでも、一旦Vimで開いてから<pre>:! poedit % &</pre>などでPoeditで開くようにすると、ファイルを探す手間が省けて効率が上がります。
