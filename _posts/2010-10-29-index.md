---
tags: []
title: 第三者割当増資実施のお知らせ
---
2010年8月31日付けで[第三者割当増資を実施](/press-releases/20101029-allocation-of-new-shares-to-third-parties.html)しました。
<!--more-->


割当先の[コスモエア株式会社](http://www.cosmoair.co.jp/)、[有限会社未来検索ブラジル](http://razil.jp/)、[ミラクル・リナックス株式会社](http://www.miraclelinux.com/)の3社とはこれまで以上に連携を強め、さらなる事業展開を積極的に実施していきます。
